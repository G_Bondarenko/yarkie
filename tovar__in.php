<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="tovar" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <?php include('parts/tovar__list.php'); ?>
            <?php include('parts/specials__block.php'); ?>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>