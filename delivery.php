<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="delivery" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <div class="delivery">
				<div class="wrapper">
					<h1>Доставка <a href="javascript:void(0);" class="mobile__display hide__filter">Свернуть</a></h1>	
				</div>
				<?php include('parts/delivery__inner.php'); ?>
			</div>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>