"use strict";

var cities_list = {"Москва":{"li":"<li><a href=\"\/russia\/655\/8\/1\/russia\" id=\"city_link_1\">Москва и область<\/a><\/li>","n":1},"Санкт-Петербург":{"li":"<li><a href=\"\/russia\/655\/9\/3\/russia\" id=\"city_link_3\">Санкт-Петербург и область<\/a><\/li>","n":3},"Абакан":{"li":"<li><a href=\"\/russia\/655\/87\/43\/russia\" id=\"city_link_43\">Абакан<\/a><\/li>","n":43},"Азов":{"li":"<li><a href=\"\/russia\/655\/19\/268\/russia\" id=\"city_link_268\">Азов<\/a><\/li>","n":268},"Алапаевск":{"li":"<li><a href=\"\/russia\/655\/4\/299\/russia\" id=\"city_link_299\">Алапаевск<\/a><\/li>","n":299},"Александров":{"li":"<li><a href=\"\/russia\/655\/38\/198\/russia\" id=\"city_link_198\">Александров<\/a><\/li>","n":198},"Алексин":{"li":"<li><a href=\"\/russia\/655\/34\/214\/russia\" id=\"city_link_214\">Алексин<\/a><\/li>","n":214},"Альметьевск":{"li":"<li><a href=\"\/russia\/655\/32\/158\/russia\" id=\"city_link_158\">Альметьевск<\/a><\/li>","n":158},"Анадырь":{"li":"<li><a href=\"\/russia\/655\/88\/44\/russia\" id=\"city_link_44\">Анадырь<\/a><\/li>","n":44},"Анапа":{"li":"<li><a href=\"\/russia\/655\/13\/45\/russia\" id=\"city_link_45\">Анапа<\/a><\/li>","n":45},"Апатиты":{"li":"<li><a href=\"\/russia\/655\/45\/46\/russia\" id=\"city_link_46\">Апатиты<\/a><\/li>","n":46},"Арзамас":{"li":"<li><a href=\"\/russia\/655\/16\/314\/russia\" id=\"city_link_314\">Арзамас<\/a><\/li>","n":314},"Армавир":{"li":"<li><a href=\"\/russia\/655\/13\/77\/russia\" id=\"city_link_77\">Армавир<\/a><\/li>","n":77},"Артем":{"li":"<li><a href=\"\/russia\/655\/50\/247\/russia\" id=\"city_link_247\">Артем<\/a><\/li>","n":247},"Архангельск":{"li":"<li><a href=\"\/russia\/655\/46\/47\/russia\" id=\"city_link_47\">Архангельск<\/a><\/li>","n":47},"Астрахань":{"li":"<li><a href=\"\/russia\/655\/10\/31\/russia\" id=\"city_link_31\">Астрахань<\/a><\/li>","n":31},"Ачинск":{"li":"<li><a href=\"\/russia\/655\/59\/226\/russia\" id=\"city_link_226\">Ачинск<\/a><\/li>","n":226},"Балабаново":{"li":"<li><a href=\"\/russia\/655\/39\/157\/russia\" id=\"city_link_157\">Балабаново<\/a><\/li>","n":157},"Балаково":{"li":"<li><a href=\"\/russia\/655\/36\/227\/russia\" id=\"city_link_227\">Балаково<\/a><\/li>","n":227},"Балахна":{"li":"<li><a href=\"\/russia\/655\/16\/271\/russia\" id=\"city_link_271\">Балахна<\/a><\/li>","n":271},"Барнаул":{"li":"<li><a href=\"\/russia\/655\/47\/48\/russia\" id=\"city_link_48\">Барнаул<\/a><\/li>","n":48},"Батайск":{"li":"<li><a href=\"\/russia\/655\/19\/319\/russia\" id=\"city_link_319\">Батайск<\/a><\/li>","n":319},"Белгород":{"li":"<li><a href=\"\/russia\/655\/48\/49\/russia\" id=\"city_link_49\">Белгород<\/a><\/li>","n":49},"Белово":{"li":"<li><a href=\"\/russia\/655\/57\/19\/russia\" id=\"city_link_19\">Белово<\/a><\/li>","n":19},"Белоярский":{"li":"<li><a href=\"\/russia\/655\/80\/50\/russia\" id=\"city_link_50\">Белоярский<\/a><\/li>","n":50},"Бердск":{"li":"<li><a href=\"\/russia\/655\/67\/320\/russia\" id=\"city_link_320\">Бердск<\/a><\/li>","n":320},"Березники":{"li":"<li><a href=\"\/russia\/655\/70\/199\/russia\" id=\"city_link_199\">Березники<\/a><\/li>","n":199},"Березовский":{"li":"<li><a href=\"\/russia\/655\/4\/294\/russia\" id=\"city_link_294\">Березовский<\/a><\/li>","n":294},"Бийск":{"li":"<li><a href=\"\/russia\/655\/47\/229\/russia\" id=\"city_link_229\">Бийск<\/a><\/li>","n":229},"Биробиджан":{"li":"<li><a href=\"\/russia\/655\/79\/27\/russia\" id=\"city_link_27\">Биробиджан<\/a><\/li>","n":27},"Благовещенск":{"li":"<li><a href=\"\/russia\/655\/89\/51\/russia\" id=\"city_link_51\">Благовещенск<\/a><\/li>","n":51},"Богородицк":{"li":"<li><a href=\"\/russia\/655\/34\/215\/russia\" id=\"city_link_215\">Богородицк<\/a><\/li>","n":215},"Бор\r\n":{"li":"<li><a href=\"\/russia\/655\/16\/156\/russia\" id=\"city_link_156\">Бор\r\n<\/a><\/li>","n":156},"Братск":{"li":"<li><a href=\"\/russia\/655\/54\/52\/russia\" id=\"city_link_52\">Братск<\/a><\/li>","n":52},"Брянск":{"li":"<li><a href=\"\/russia\/655\/49\/78\/russia\" id=\"city_link_78\">Брянск<\/a><\/li>","n":78},"Бугульма":{"li":"<li><a href=\"\/russia\/655\/32\/53\/russia\" id=\"city_link_53\">Бугульма<\/a><\/li>","n":53},"Бугуруслан":{"li":"<li><a href=\"\/russia\/655\/69\/196\/russia\" id=\"city_link_196\">Бугуруслан<\/a><\/li>","n":196},"Бузулук":{"li":"<li><a href=\"\/russia\/655\/69\/276\/russia\" id=\"city_link_276\">Бузулук<\/a><\/li>","n":276},"Великие Луки":{"li":"<li><a href=\"\/russia\/655\/72\/250\/russia\" id=\"city_link_250\">Великие Луки<\/a><\/li>","n":250},"Великий Новгород":{"li":"<li><a href=\"\/russia\/655\/66\/39\/russia\" id=\"city_link_39\">Великий Новгород<\/a><\/li>","n":39},"Венев":{"li":"<li><a href=\"\/russia\/655\/34\/216\/russia\" id=\"city_link_216\">Венев<\/a><\/li>","n":216},"Верхняя Пышма":{"li":"<li><a href=\"\/russia\/655\/4\/155\/russia\" id=\"city_link_155\">Верхняя Пышма<\/a><\/li>","n":155},"Верхняя Салда":{"li":"<li><a href=\"\/russia\/655\/4\/324\/russia\" id=\"city_link_324\">Верхняя Салда<\/a><\/li>","n":324},"Владивосток":{"li":"<li><a href=\"\/russia\/655\/50\/5\/russia\" id=\"city_link_5\">Владивосток<\/a><\/li>","n":5},"Владикавказ":{"li":"<li><a href=\"\/russia\/655\/51\/54\/russia\" id=\"city_link_54\">Владикавказ<\/a><\/li>","n":54},"Владимир":{"li":"<li><a href=\"\/russia\/655\/38\/154\/russia\" id=\"city_link_154\">Владимир<\/a><\/li>","n":154},"Волгоград":{"li":"<li><a href=\"\/russia\/655\/11\/55\/russia\" id=\"city_link_55\">Волгоград<\/a><\/li>","n":55},"Волгодонск":{"li":"<li><a href=\"\/russia\/655\/19\/153\/russia\" id=\"city_link_153\">Волгодонск<\/a><\/li>","n":153},"Волжский":{"li":"<li><a href=\"\/russia\/655\/11\/79\/russia\" id=\"city_link_79\">Волжский<\/a><\/li>","n":79},"Вологда":{"li":"<li><a href=\"\/russia\/655\/52\/6\/russia\" id=\"city_link_6\">Вологда<\/a><\/li>","n":6},"Воркута":{"li":"<li><a href=\"\/russia\/655\/74\/203\/russia\" id=\"city_link_203\">Воркута<\/a><\/li>","n":203},"Воронеж":{"li":"<li><a href=\"\/russia\/655\/30\/56\/russia\" id=\"city_link_56\">Воронеж<\/a><\/li>","n":56},"Воткинск":{"li":"<li><a href=\"\/russia\/655\/33\/14\/russia\" id=\"city_link_14\">Воткинск<\/a><\/li>","n":14},"Всеволожск":{"li":"<li><a href=\"\/russia\/655\/9\/152\/russia\" id=\"city_link_152\">Всеволожск<\/a><\/li>","n":152},"Выборг":{"li":"<li><a href=\"\/russia\/655\/9\/151\/russia\" id=\"city_link_151\">Выборг<\/a><\/li>","n":151},"Геленджик":{"li":"<li><a href=\"\/russia\/655\/13\/80\/russia\" id=\"city_link_80\">Геленджик<\/a><\/li>","n":80},"Глазов":{"li":"<li><a href=\"\/russia\/655\/33\/210\/russia\" id=\"city_link_210\">Глазов<\/a><\/li>","n":210},"Грозный":{"li":"<li><a href=\"\/russia\/655\/53\/168\/russia\" id=\"city_link_168\">Грозный<\/a><\/li>","n":168},"Дзержинск":{"li":"<li><a href=\"\/russia\/655\/16\/150\/russia\" id=\"city_link_150\">Дзержинск<\/a><\/li>","n":150},"Димитровград":{"li":"<li><a href=\"\/russia\/655\/23\/149\/russia\" id=\"city_link_149\">Димитровград<\/a><\/li>","n":149},"Донской":{"li":"<li><a href=\"\/russia\/655\/34\/217\/russia\" id=\"city_link_217\">Донской<\/a><\/li>","n":217},"Евпатория":{"li":"<li><a href=\"\/russia\/655\/96\/279\/russia\" id=\"city_link_279\">Евпатория<\/a><\/li>","n":279},"Екатеринбург":{"li":"<li><a href=\"\/russia\/655\/4\/57\/russia\" id=\"city_link_57\">Екатеринбург<\/a><\/li>","n":57},"Елабуга":{"li":"<li><a href=\"\/russia\/655\/32\/148\/russia\" id=\"city_link_148\">Елабуга<\/a><\/li>","n":148},"Елец":{"li":"<li><a href=\"\/russia\/655\/62\/147\/russia\" id=\"city_link_147\">Елец<\/a><\/li>","n":147},"Ессентуки":{"li":"<li><a href=\"\/russia\/655\/21\/84\/russia\" id=\"city_link_84\">Ессентуки<\/a><\/li>","n":84},"Железногорск":{"li":"<li><a href=\"\/russia\/655\/61\/321\/russia\" id=\"city_link_321\">Железногорск<\/a><\/li>","n":321},"Заречный":{"li":"<li><a href=\"\/russia\/655\/4\/161\/russia\" id=\"city_link_161\">Заречный<\/a><\/li>","n":161},"Златоуст":{"li":"<li><a href=\"\/russia\/655\/24\/231\/russia\" id=\"city_link_231\">Златоуст<\/a><\/li>","n":231},"Иваново":{"li":"<li><a href=\"\/russia\/655\/44\/146\/russia\" id=\"city_link_146\">Иваново<\/a><\/li>","n":146},"Ижевск":{"li":"<li><a href=\"\/russia\/655\/33\/58\/russia\" id=\"city_link_58\">Ижевск<\/a><\/li>","n":58},"Иркутск":{"li":"<li><a href=\"\/russia\/655\/54\/60\/russia\" id=\"city_link_60\">Иркутск<\/a><\/li>","n":60},"Ишим":{"li":"<li><a href=\"\/russia\/655\/29\/308\/russia\" id=\"city_link_308\">Ишим<\/a><\/li>","n":308},"Йошкар-Ола":{"li":"<li><a href=\"\/russia\/655\/55\/145\/russia\" id=\"city_link_145\">Йошкар-Ола<\/a><\/li>","n":145},"Казань":{"li":"<li><a href=\"\/russia\/655\/32\/61\/russia\" id=\"city_link_61\">Казань<\/a><\/li>","n":61},"Калининград":{"li":"<li><a href=\"\/russia\/655\/56\/62\/russia\" id=\"city_link_62\">Калининград<\/a><\/li>","n":62},"Калуга":{"li":"<li><a href=\"\/russia\/655\/39\/13\/russia\" id=\"city_link_13\">Калуга<\/a><\/li>","n":13},"Каменск-Уральский":{"li":"<li><a href=\"\/russia\/655\/4\/205\/russia\" id=\"city_link_205\">Каменск-Уральский<\/a><\/li>","n":205},"Камышин":{"li":"<li><a href=\"\/russia\/655\/11\/252\/russia\" id=\"city_link_252\">Камышин<\/a><\/li>","n":252},"Канск":{"li":"<li><a href=\"\/russia\/655\/59\/309\/russia\" id=\"city_link_309\">Канск<\/a><\/li>","n":309},"Качканар":{"li":"<li><a href=\"\/russia\/655\/4\/278\/russia\" id=\"city_link_278\">Качканар<\/a><\/li>","n":278},"Кемерово":{"li":"<li><a href=\"\/russia\/655\/57\/63\/russia\" id=\"city_link_63\">Кемерово<\/a><\/li>","n":63},"Керчь":{"li":"<li><a href=\"\/russia\/655\/96\/260\/russia\" id=\"city_link_260\">Керчь<\/a><\/li>","n":260},"Кимовск":{"li":"<li><a href=\"\/russia\/655\/34\/219\/russia\" id=\"city_link_219\">Кимовск<\/a><\/li>","n":219},"Кинешма":{"li":"<li><a href=\"\/russia\/655\/44\/289\/russia\" id=\"city_link_289\">Кинешма<\/a><\/li>","n":289},"Киров":{"li":"<li><a href=\"\/russia\/655\/58\/32\/russia\" id=\"city_link_32\">Киров<\/a><\/li>","n":32},"Кирово-Чепецк":{"li":"<li><a href=\"\/russia\/655\/34\/277\/russia\" id=\"city_link_277\">Кирово-Чепецк<\/a><\/li>","n":277},"Киселевск":{"li":"<li><a href=\"\/russia\/655\/57\/21\/russia\" id=\"city_link_21\">Киселевск<\/a><\/li>","n":21},"Кисловодск":{"li":"<li><a href=\"\/russia\/655\/21\/87\/russia\" id=\"city_link_87\">Кисловодск<\/a><\/li>","n":87},"Ковров":{"li":"<li><a href=\"\/russia\/655\/38\/197\/russia\" id=\"city_link_197\">Ковров<\/a><\/li>","n":197},"Когалым":{"li":"<li><a href=\"\/russia\/655\/80\/64\/russia\" id=\"city_link_64\">Когалым<\/a><\/li>","n":64},"Комсомольск-на-Амуре":{"li":"<li><a href=\"\/russia\/655\/79\/65\/russia\" id=\"city_link_65\">Комсомольск-на-Амуре<\/a><\/li>","n":65},"Конаково":{"li":"<li><a href=\"\/russia\/655\/76\/290\/russia\" id=\"city_link_290\">Конаково<\/a><\/li>","n":290},"Коркино":{"li":"<li><a href=\"\/russia\/655\/24\/295\/russia\" id=\"city_link_295\">Коркино<\/a><\/li>","n":295},"Костомукша":{"li":"<li><a href=\"\/russia\/655\/71\/232\/russia\" id=\"city_link_232\">Костомукша<\/a><\/li>","n":232},"Кострома":{"li":"<li><a href=\"\/russia\/655\/41\/144\/russia\" id=\"city_link_144\">Кострома<\/a><\/li>","n":144},"Краснодар":{"li":"<li><a href=\"\/russia\/655\/13\/40\/russia\" id=\"city_link_40\">Краснодар<\/a><\/li>","n":40},"Краснотурьинск":{"li":"<li><a href=\"\/russia\/655\/4\/243\/russia\" id=\"city_link_243\">Краснотурьинск<\/a><\/li>","n":243},"Красноуфимск":{"li":"<li><a href=\"\/russia\/655\/4\/313\/russia\" id=\"city_link_313\">Красноуфимск<\/a><\/li>","n":313},"Красноярск":{"li":"<li><a href=\"\/russia\/655\/59\/66\/russia\" id=\"city_link_66\">Красноярск<\/a><\/li>","n":66},"Кропоткин":{"li":"<li><a href=\"\/russia\/655\/13\/88\/russia\" id=\"city_link_88\">Кропоткин<\/a><\/li>","n":88},"Кстово":{"li":"<li><a href=\"\/russia\/655\/16\/293\/russia\" id=\"city_link_293\">Кстово<\/a><\/li>","n":293},"Кумертау ":{"li":"<li><a href=\"\/russia\/655\/31\/307\/russia\" id=\"city_link_307\">Кумертау <\/a><\/li>","n":307},"Курган":{"li":"<li><a href=\"\/russia\/655\/60\/67\/russia\" id=\"city_link_67\">Курган<\/a><\/li>","n":67},"Курганинск":{"li":"<li><a href=\"\/russia\/655\/13\/90\/russia\" id=\"city_link_90\">Курганинск<\/a><\/li>","n":90},"Курск":{"li":"<li><a href=\"\/russia\/655\/61\/91\/russia\" id=\"city_link_91\">Курск<\/a><\/li>","n":91},"Лениногорск":{"li":"<li><a href=\"\/russia\/655\/32\/187\/russia\" id=\"city_link_187\">Лениногорск<\/a><\/li>","n":187},"Ленинск-Кузнецкий":{"li":"<li><a href=\"\/russia\/655\/57\/296\/russia\" id=\"city_link_296\">Ленинск-Кузнецкий<\/a><\/li>","n":296},"Лесной":{"li":"<li><a href=\"\/russia\/655\/4\/206\/russia\" id=\"city_link_206\">Лесной<\/a><\/li>","n":206},"Липецк":{"li":"<li><a href=\"\/russia\/655\/62\/93\/russia\" id=\"city_link_93\">Липецк<\/a><\/li>","n":93},"Лиски":{"li":"<li><a href=\"\/russia\/655\/30\/315\/russia\" id=\"city_link_315\">Лиски<\/a><\/li>","n":315},"Лысьва":{"li":"<li><a href=\"\/russia\/655\/70\/259\/russia\" id=\"city_link_259\">Лысьва<\/a><\/li>","n":259},"Магадан":{"li":"<li><a href=\"\/russia\/655\/63\/68\/russia\" id=\"city_link_68\">Магадан<\/a><\/li>","n":68},"Магнитогорск":{"li":"<li><a href=\"\/russia\/655\/24\/69\/russia\" id=\"city_link_69\">Магнитогорск<\/a><\/li>","n":69},"Майкоп":{"li":"<li><a href=\"\/russia\/655\/13\/94\/russia\" id=\"city_link_94\">Майкоп<\/a><\/li>","n":94},"Махачкала":{"li":"<li><a href=\"\/russia\/655\/64\/70\/russia\" id=\"city_link_70\">Махачкала<\/a><\/li>","n":70},"Междуреченск":{"li":"<li><a href=\"\/russia\/655\/57\/233\/russia\" id=\"city_link_233\">Междуреченск<\/a><\/li>","n":233},"Мелеуз ":{"li":"<li><a href=\"\/russia\/655\/31\/306\/russia\" id=\"city_link_306\">Мелеуз <\/a><\/li>","n":306},"Миасс":{"li":"<li><a href=\"\/russia\/655\/24\/211\/russia\" id=\"city_link_211\">Миасс<\/a><\/li>","n":211},"Минеральные Воды":{"li":"<li><a href=\"\/russia\/655\/21\/71\/russia\" id=\"city_link_71\">Минеральные Воды<\/a><\/li>","n":71},"Минусинск":{"li":"<li><a href=\"\/russia\/655\/59\/304\/russia\" id=\"city_link_304\">Минусинск<\/a><\/li>","n":304},"Мирный":{"li":"<li><a href=\"\/russia\/655\/84\/72\/russia\" id=\"city_link_72\">Мирный<\/a><\/li>","n":72},"Михайловка":{"li":"<li><a href=\"\/russia\/655\/11\/272\/russia\" id=\"city_link_272\">Михайловка<\/a><\/li>","n":272},"Мичуринск":{"li":"<li><a href=\"\/russia\/655\/75\/265\/russia\" id=\"city_link_265\">Мичуринск<\/a><\/li>","n":265},"Мурманск":{"li":"<li><a href=\"\/russia\/655\/45\/73\/russia\" id=\"city_link_73\">Мурманск<\/a><\/li>","n":73},"Муром":{"li":"<li><a href=\"\/russia\/655\/38\/162\/russia\" id=\"city_link_162\">Муром<\/a><\/li>","n":162},"Набережные Челны":{"li":"<li><a href=\"\/russia\/655\/32\/96\/russia\" id=\"city_link_96\">Набережные Челны<\/a><\/li>","n":96},"Надым":{"li":"<li><a href=\"\/russia\/655\/86\/170\/russia\" id=\"city_link_170\">Надым<\/a><\/li>","n":170},"Нальчик":{"li":"<li><a href=\"\/russia\/655\/65\/143\/russia\" id=\"city_link_143\">Нальчик<\/a><\/li>","n":143},"Находка":{"li":"<li><a href=\"\/russia\/655\/50\/142\/russia\" id=\"city_link_142\">Находка<\/a><\/li>","n":142},"Невинномысск":{"li":"<li><a href=\"\/russia\/655\/21\/262\/russia\" id=\"city_link_262\">Невинномысск<\/a><\/li>","n":262},"Нерюнгри ":{"li":"<li><a href=\"\/russia\/655\/84\/171\/russia\" id=\"city_link_171\">Нерюнгри <\/a><\/li>","n":171},"Нефтекамск":{"li":"<li><a href=\"\/russia\/655\/31\/201\/russia\" id=\"city_link_201\">Нефтекамск<\/a><\/li>","n":201},"Нефтеюганск":{"li":"<li><a href=\"\/russia\/655\/80\/141\/russia\" id=\"city_link_141\">Нефтеюганск<\/a><\/li>","n":141},"Нижневартовск":{"li":"<li><a href=\"\/russia\/655\/84\/140\/russia\" id=\"city_link_140\">Нижневартовск<\/a><\/li>","n":140},"Нижнекамск":{"li":"<li><a href=\"\/russia\/655\/32\/20\/russia\" id=\"city_link_20\">Нижнекамск<\/a><\/li>","n":20},"Нижний Новгород":{"li":"<li><a href=\"\/russia\/655\/16\/25\/russia\" id=\"city_link_25\">Нижний Новгород<\/a><\/li>","n":25},"Нижний Тагил":{"li":"<li><a href=\"\/russia\/655\/4\/139\/russia\" id=\"city_link_139\">Нижний Тагил<\/a><\/li>","n":139},"Нижняя Тура":{"li":"<li><a href=\"\/russia\/655\/4\/323\/russia\" id=\"city_link_323\">Нижняя Тура<\/a><\/li>","n":323},"Новоалтайск":{"li":"<li><a href=\"\/russia\/655\/47\/322\/russia\" id=\"city_link_322\">Новоалтайск<\/a><\/li>","n":322},"Новокузнецк":{"li":"<li><a href=\"\/russia\/655\/57\/97\/russia\" id=\"city_link_97\">Новокузнецк<\/a><\/li>","n":97},"Новокуйбышевск":{"li":"<li><a href=\"\/russia\/655\/20\/280\/russia\" id=\"city_link_280\">Новокуйбышевск<\/a><\/li>","n":280},"Новомосковск":{"li":"<li><a href=\"\/russia\/655\/34\/221\/russia\" id=\"city_link_221\">Новомосковск<\/a><\/li>","n":221},"Новороссийск":{"li":"<li><a href=\"\/russia\/655\/13\/99\/russia\" id=\"city_link_99\">Новороссийск<\/a><\/li>","n":99},"Новосибирск":{"li":"<li><a href=\"\/russia\/655\/67\/36\/russia\" id=\"city_link_36\">Новосибирск<\/a><\/li>","n":36},"Новоуральск":{"li":"<li><a href=\"\/russia\/655\/4\/207\/russia\" id=\"city_link_207\">Новоуральск<\/a><\/li>","n":207},"Новочебоксарск":{"li":"<li><a href=\"\/russia\/655\/81\/301\/russia\" id=\"city_link_301\">Новочебоксарск<\/a><\/li>","n":301},"Новочеркасск":{"li":"<li><a href=\"\/russia\/655\/19\/9\/russia\" id=\"city_link_9\">Новочеркасск<\/a><\/li>","n":9},"Новый Уренгой":{"li":"<li><a href=\"\/russia\/655\/29\/166\/russia\" id=\"city_link_166\">Новый Уренгой<\/a><\/li>","n":166},"Норильск":{"li":"<li><a href=\"\/russia\/655\/59\/172\/russia\" id=\"city_link_172\">Норильск<\/a><\/li>","n":172},"Ноябрьск":{"li":"<li><a href=\"\/russia\/655\/86\/173\/russia\" id=\"city_link_173\">Ноябрьск<\/a><\/li>","n":173},"Нягань":{"li":"<li><a href=\"\/russia\/655\/80\/297\/russia\" id=\"city_link_297\">Нягань<\/a><\/li>","n":297},"Обнинск":{"li":"<li><a href=\"\/russia\/655\/39\/138\/russia\" id=\"city_link_138\">Обнинск<\/a><\/li>","n":138},"Обь":{"li":"<li><a href=\"\/russia\/655\/67\/137\/russia\" id=\"city_link_137\">Обь<\/a><\/li>","n":137},"Озерск":{"li":"<li><a href=\"\/russia\/655\/24\/212\/russia\" id=\"city_link_212\">Озерск<\/a><\/li>","n":212},"Октябрьский":{"li":"<li><a href=\"\/russia\/655\/31\/242\/russia\" id=\"city_link_242\">Октябрьский<\/a><\/li>","n":242},"Омск":{"li":"<li><a href=\"\/russia\/655\/40\/37\/russia\" id=\"city_link_37\">Омск<\/a><\/li>","n":37},"Орел":{"li":"<li><a href=\"\/russia\/655\/68\/100\/russia\" id=\"city_link_100\">Орел<\/a><\/li>","n":100},"Оренбург":{"li":"<li><a href=\"\/russia\/655\/69\/10\/russia\" id=\"city_link_10\">Оренбург<\/a><\/li>","n":10},"Орск":{"li":"<li><a href=\"\/russia\/655\/69\/234\/russia\" id=\"city_link_234\">Орск<\/a><\/li>","n":234},"Осташков":{"li":"<li><a href=\"\/russia\/655\/76\/288\/russia\" id=\"city_link_288\">Осташков<\/a><\/li>","n":288},"Пенза":{"li":"<li><a href=\"\/russia\/655\/3\/4\/russia\" id=\"city_link_4\">Пенза<\/a><\/li>","n":4},"Первоуральск":{"li":"<li><a href=\"\/russia\/655\/4\/208\/russia\" id=\"city_link_208\">Первоуральск<\/a><\/li>","n":208},"Пермь":{"li":"<li><a href=\"\/russia\/655\/70\/41\/russia\" id=\"city_link_41\">Пермь<\/a><\/li>","n":41},"Петрозаводск":{"li":"<li><a href=\"\/russia\/655\/71\/136\/russia\" id=\"city_link_136\">Петрозаводск<\/a><\/li>","n":136},"Петропавловск-Камчатский":{"li":"<li><a href=\"\/russia\/655\/85\/135\/russia\" id=\"city_link_135\">Петропавловск-Камчатский<\/a><\/li>","n":135},"Печоры":{"li":"<li><a href=\"\/russia\/655\/72\/134\/russia\" id=\"city_link_134\">Печоры<\/a><\/li>","n":134},"Прокопьевск":{"li":"<li><a href=\"\/russia\/655\/57\/235\/russia\" id=\"city_link_235\">Прокопьевск<\/a><\/li>","n":235},"Псков":{"li":"<li><a href=\"\/russia\/655\/72\/101\/russia\" id=\"city_link_101\">Псков<\/a><\/li>","n":101},"Пятигорск":{"li":"<li><a href=\"\/russia\/655\/21\/102\/russia\" id=\"city_link_102\">Пятигорск<\/a><\/li>","n":102},"Ревда":{"li":"<li><a href=\"\/russia\/655\/4\/318\/russia\" id=\"city_link_318\">Ревда<\/a><\/li>","n":318},"Репино":{"li":"<li><a href=\"\/russia\/655\/9\/133\/russia\" id=\"city_link_133\">Репино<\/a><\/li>","n":133},"Ростов-на-Дону":{"li":"<li><a href=\"\/russia\/655\/19\/8\/russia\" id=\"city_link_8\">Ростов-на-Дону<\/a><\/li>","n":8},"Рубцовск":{"li":"<li><a href=\"\/russia\/655\/47\/263\/russia\" id=\"city_link_263\">Рубцовск<\/a><\/li>","n":263},"Рыбинск":{"li":"<li><a href=\"\/russia\/655\/25\/236\/russia\" id=\"city_link_236\">Рыбинск<\/a><\/li>","n":236},"Рязань":{"li":"<li><a href=\"\/russia\/655\/35\/28\/russia\" id=\"city_link_28\">Рязань<\/a><\/li>","n":28},"Салават":{"li":"<li><a href=\"\/russia\/655\/31\/202\/russia\" id=\"city_link_202\">Салават<\/a><\/li>","n":202},"Салехард":{"li":"<li><a href=\"\/russia\/655\/86\/174\/russia\" id=\"city_link_174\">Салехард<\/a><\/li>","n":174},"Самара":{"li":"<li><a href=\"\/russia\/655\/20\/103\/russia\" id=\"city_link_103\">Самара и область<\/a><\/li>","n":103},"Саранск":{"li":"<li><a href=\"\/russia\/655\/73\/132\/russia\" id=\"city_link_132\">Саранск<\/a><\/li>","n":132},"Сарапул":{"li":"<li><a href=\"\/russia\/655\/33\/16\/russia\" id=\"city_link_16\">Сарапул<\/a><\/li>","n":16},"Саратов":{"li":"<li><a href=\"\/russia\/655\/36\/104\/russia\" id=\"city_link_104\">Саратов<\/a><\/li>","n":104},"Саратовская область":{"li":"<li><a href=\"\/russia\/655\/36\/312\/russia\" id=\"city_link_312\">Саратовская область<\/a><\/li>","n":312},"Саров":{"li":"<li><a href=\"\/russia\/655\/16\/317\/russia\" id=\"city_link_317\">Саров<\/a><\/li>","n":317},"Светогорск":{"li":"<li><a href=\"\/russia\/655\/9\/300\/russia\" id=\"city_link_300\">Светогорск<\/a><\/li>","n":300},"Севастополь":{"li":"<li><a href=\"\/russia\/655\/96\/261\/russia\" id=\"city_link_261\">Севастополь<\/a><\/li>","n":261},"Северодвинск":{"li":"<li><a href=\"\/russia\/655\/46\/258\/russia\" id=\"city_link_258\">Северодвинск<\/a><\/li>","n":258},"Северск":{"li":"<li><a href=\"\/russia\/655\/77\/237\/russia\" id=\"city_link_237\">Северск<\/a><\/li>","n":237},"Серов":{"li":"<li><a href=\"\/russia\/655\/4\/209\/russia\" id=\"city_link_209\">Серов<\/a><\/li>","n":209},"Симферополь":{"li":"<li><a href=\"\/russia\/655\/96\/256\/russia\" id=\"city_link_256\">Симферополь<\/a><\/li>","n":256},"Смоленск":{"li":"<li><a href=\"\/russia\/655\/43\/38\/russia\" id=\"city_link_38\">Смоленск<\/a><\/li>","n":38},"Снежинск":{"li":"<li><a href=\"\/russia\/655\/24\/213\/russia\" id=\"city_link_213\">Снежинск<\/a><\/li>","n":213},"Советск":{"li":"<li><a href=\"\/russia\/655\/34\/287\/russia\" id=\"city_link_287\">Советск<\/a><\/li>","n":287},"Советский":{"li":"<li><a href=\"\/russia\/655\/80\/255\/russia\" id=\"city_link_255\">Советский<\/a><\/li>","n":255},"Сочи":{"li":"<li><a href=\"\/russia\/655\/13\/106\/russia\" id=\"city_link_106\">Сочи<\/a><\/li>","n":106},"Ставрополь":{"li":"<li><a href=\"\/russia\/655\/21\/107\/russia\" id=\"city_link_107\">Ставрополь<\/a><\/li>","n":107},"Старый Оскол":{"li":"<li><a href=\"\/russia\/655\/48\/108\/russia\" id=\"city_link_108\">Старый Оскол<\/a><\/li>","n":108},"Стерлитамак":{"li":"<li><a href=\"\/russia\/655\/31\/131\/russia\" id=\"city_link_131\">Стерлитамак<\/a><\/li>","n":131},"Стрежевой":{"li":"<li><a href=\"\/russia\/655\/77\/292\/russia\" id=\"city_link_292\">Стрежевой<\/a><\/li>","n":292},"Судак":{"li":"<li><a href=\"\/russia\/655\/96\/273\/russia\" id=\"city_link_273\">Судак<\/a><\/li>","n":273},"Сургут":{"li":"<li><a href=\"\/russia\/655\/80\/130\/russia\" id=\"city_link_130\">Сургут<\/a><\/li>","n":130},"Сызрань":{"li":"<li><a href=\"\/russia\/655\/20\/11\/russia\" id=\"city_link_11\">Сызрань<\/a><\/li>","n":11},"Сыктывкар":{"li":"<li><a href=\"\/russia\/655\/74\/22\/russia\" id=\"city_link_22\">Сыктывкар<\/a><\/li>","n":22},"Сысерть":{"li":"<li><a href=\"\/russia\/655\/4\/270\/russia\" id=\"city_link_270\">Сысерть<\/a><\/li>","n":270},"Таганрог":{"li":"<li><a href=\"\/russia\/655\/19\/129\/russia\" id=\"city_link_129\">Таганрог<\/a><\/li>","n":129},"Тамбов":{"li":"<li><a href=\"\/russia\/655\/75\/128\/russia\" id=\"city_link_128\">Тамбов<\/a><\/li>","n":128},"Тверь":{"li":"<li><a href=\"\/russia\/655\/76\/119\/russia\" id=\"city_link_119\">Тверь<\/a><\/li>","n":119},"Тихорецк":{"li":"<li><a href=\"\/russia\/655\/13\/127\/russia\" id=\"city_link_127\">Тихорецк<\/a><\/li>","n":127},"Тобольск":{"li":"<li><a href=\"\/russia\/655\/29\/302\/russia\" id=\"city_link_302\">Тобольск<\/a><\/li>","n":302},"Томск":{"li":"<li><a href=\"\/russia\/655\/77\/110\/russia\" id=\"city_link_110\">Томск<\/a><\/li>","n":110},"Туймазы ":{"li":"<li><a href=\"\/russia\/655\/31\/305\/russia\" id=\"city_link_305\">Туймазы <\/a><\/li>","n":305},"Тула":{"li":"<li><a href=\"\/russia\/655\/34\/111\/russia\" id=\"city_link_111\">Тула<\/a><\/li>","n":111},"Тюмень":{"li":"<li><a href=\"\/russia\/655\/29\/112\/russia\" id=\"city_link_112\">Тюмень<\/a><\/li>","n":112},"Углич":{"li":"<li><a href=\"\/russia\/655\/25\/274\/russia\" id=\"city_link_274\">Углич<\/a><\/li>","n":274},"Узловая":{"li":"<li><a href=\"\/russia\/655\/34\/222\/russia\" id=\"city_link_222\">Узловая<\/a><\/li>","n":222},"Улан-Удэ":{"li":"<li><a href=\"\/russia\/655\/78\/126\/russia\" id=\"city_link_126\">Улан-Удэ<\/a><\/li>","n":126},"Ульяновск":{"li":"<li><a href=\"\/russia\/655\/23\/125\/russia\" id=\"city_link_125\">Ульяновск<\/a><\/li>","n":125},"Уссурийск":{"li":"<li><a href=\"\/russia\/655\/50\/239\/russia\" id=\"city_link_239\">Уссурийск<\/a><\/li>","n":239},"Уфа":{"li":"<li><a href=\"\/russia\/655\/31\/114\/russia\" id=\"city_link_114\">Уфа<\/a><\/li>","n":114},"Ухта":{"li":"<li><a href=\"\/russia\/655\/74\/24\/russia\" id=\"city_link_24\">Ухта<\/a><\/li>","n":24},"Феодосия":{"li":"<li><a href=\"\/russia\/655\/96\/291\/russia\" id=\"city_link_291\">Феодосия<\/a><\/li>","n":291},"Хабаровск":{"li":"<li><a href=\"\/russia\/655\/79\/124\/russia\" id=\"city_link_124\">Хабаровск<\/a><\/li>","n":124},"Ханты-Мансийск":{"li":"<li><a href=\"\/russia\/655\/80\/123\/russia\" id=\"city_link_123\">Ханты-Мансийск<\/a><\/li>","n":123},"Чебоксары":{"li":"<li><a href=\"\/russia\/655\/81\/115\/russia\" id=\"city_link_115\">Чебоксары<\/a><\/li>","n":115},"Челябинск":{"li":"<li><a href=\"\/russia\/655\/24\/116\/russia\" id=\"city_link_116\">Челябинск<\/a><\/li>","n":116},"Череповец":{"li":"<li><a href=\"\/russia\/655\/52\/122\/russia\" id=\"city_link_122\">Череповец<\/a><\/li>","n":122},"Черногорск":{"li":"<li><a href=\"\/russia\/655\/87\/303\/russia\" id=\"city_link_303\">Черногорск<\/a><\/li>","n":303},"Чита":{"li":"<li><a href=\"\/russia\/655\/82\/121\/russia\" id=\"city_link_121\">Чита<\/a><\/li>","n":121},"Шадринск":{"li":"<li><a href=\"\/russia\/655\/60\/200\/russia\" id=\"city_link_200\">Шадринск<\/a><\/li>","n":200},"Шахты":{"li":"<li><a href=\"\/russia\/655\/19\/245\/russia\" id=\"city_link_245\">Шахты<\/a><\/li>","n":245},"Шуя":{"li":"<li><a href=\"\/russia\/655\/44\/316\/russia\" id=\"city_link_316\">Шуя<\/a><\/li>","n":316},"Щекино":{"li":"<li><a href=\"\/russia\/655\/34\/223\/russia\" id=\"city_link_223\">Щекино<\/a><\/li>","n":223},"Энгельс":{"li":"<li><a href=\"\/russia\/655\/36\/275\/russia\" id=\"city_link_275\">Энгельс<\/a><\/li>","n":275},"Югорск":{"li":"<li><a href=\"\/russia\/655\/80\/298\/russia\" id=\"city_link_298\">Югорск<\/a><\/li>","n":298},"Южно-Сахалинск":{"li":"<li><a href=\"\/russia\/655\/83\/120\/russia\" id=\"city_link_120\">Южно-Сахалинск<\/a><\/li>","n":120},"Южноуральск":{"li":"<li><a href=\"\/russia\/655\/24\/264\/russia\" id=\"city_link_264\">Южноуральск<\/a><\/li>","n":264},"Юрга":{"li":"<li><a href=\"\/russia\/655\/57\/29\/russia\" id=\"city_link_29\">Юрга<\/a><\/li>","n":29},"Якутск":{"li":"<li><a href=\"\/russia\/655\/84\/175\/russia\" id=\"city_link_175\">Якутск<\/a><\/li>","n":175},"Ялта":{"li":"<li><a href=\"\/russia\/655\/96\/269\/russia\" id=\"city_link_269\">Ялта<\/a><\/li>","n":269},"Ярославль":{"li":"<li><a href=\"\/russia\/655\/25\/117\/russia\" id=\"city_link_117\">Ярославль<\/a><\/li>","n":117}};

//Plaeholder handler
if(!Modernizr.input.placeholder){             //placeholder for old brousers and IE
 
  $('[placeholder]').focus(function() {
   	var input = $(this);
   	if (input.val() == input.attr('placeholder')) {
    	input.val('');
    	input.removeClass('placeholder');
   	}
  }).blur(function() {
   	var input = $(this);
   	if (input.val() == '' || input.val() == input.attr('placeholder')) {
    	input.addClass('placeholder');
    	input.val(input.attr('placeholder'));
   	}
  }).blur();
 
  $('[placeholder]').parents('form').submit(function() {
   	$(this).find('[placeholder]').each(function() {
    	var input = $(this);
    	if (input.val() == input.attr('placeholder')) {
     		input.val('');
    	}
   	})
  });
 }

var pgwBrowser = $.pgwBrowser();
$('html').addClass(pgwBrowser.browser.name + ' ' + pgwBrowser.browser.fullVersion + ' ' + pgwBrowser.os.name + ' ' + pgwBrowser.os.fullVersion);

if (($('html').hasClass('Windows')) && (pgwBrowser.browser.name == 'Internet Explorer')) {
    var vers = pgwBrowser.browser.fullVersion;
    vers = vers.replace(/Windows /g, '');
    vers = vers[0];
    if (vers == 8) {
        $('html').addClass('ie8');
    }
}

var header__slider = $('#header__slider').bxSlider({
    mode: 'fade',
    controls: false,
    auto: true
});

$('#header__slider').parents('.bx-wrapper').addClass('header__sliders');

if (($('html').hasClass('mobile'))) {
	var slider__block = $(".slider__block-ul");
	slider__block.owlCarousel({

		items : 1, //10 items above 1000px browser width
		itemsDesktop : [1000,1], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,1], // 3 items betweem 900px and 601px
		itemsTablet: [801,1], //2 items between 600 and 0;
		itemsMobile : [670,1], // itemsMobile disabled - inherit from itemsTablet option
		navigation : true,
		pagination: false

	});
} else {
	var slider__block = $(".slider__block-ul");
	slider__block.owlCarousel({

		items : 3, //10 items above 1000px browser width
		itemsDesktop : [1000,3], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
		itemsTablet: [801,3], //2 items between 600 and 0;
		itemsMobile : [640,3], // itemsMobile disabled - inherit from itemsTablet option
		navigation : true,
		pagination: false

	});
}

if (($('html').hasClass('mobile'))) {
	var format__block = $(".format__block-ul");
	format__block.owlCarousel({

		items : 2, //10 items above 1000px browser width
		itemsDesktop : [1000,2], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
		itemsTablet: [801,2], //2 items between 600 and 0;
		itemsMobile : [670,2], // itemsMobile disabled - inherit from itemsTablet option
		navigation : true,
		pagination: false

	});
} else {
	var format__block = $(".format__block-ul");
	format__block.owlCarousel({

		items : 5, //10 items above 1000px browser width
		itemsDesktop : [1000,5], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,5], // 3 items betweem 900px and 601px
		itemsTablet: [801,5], //2 items between 600 and 0;
		itemsMobile : [640,5], // itemsMobile disabled - inherit from itemsTablet option
		navigation : true,
		pagination: false

	});
}

$('.tovar__slider').bxSlider({
    pagerCustom: '#tovar__slider-pager',
    video: true,
    useCSS: false
});

// Custom Navigation Events
$(".next").click(function(){
	owl.trigger('owl.next');
})
$(".prev").click(function(){
	owl.trigger('owl.prev');
})
$(".play").click(function(){
	owl.trigger('owl.play',1000);
})
$(".stop").click(function(){
	owl.trigger('owl.stop');
})

if (($('html').hasClass('mobile'))) {
	if ($('#header__slider').length) {
		header__slider.reloadSlider({
			mode: 'fade',
			controls: true,
			auto: false,
			pager: false
		});
		$('#header__slider').parents('.bx-wrapper').addClass('header__sliders');
	}		
	
	var slider__block = $(".calendar__list ul");
	slider__block.owlCarousel({

		items : 2, //10 items above 1000px browser width
		itemsDesktop : [1000,2], //5 items between 1000px and 901px
		itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
		itemsTablet: [800,2], //2 items between 600 and 0;
		itemsMobile : [670,2], // itemsMobile disabled - inherit from itemsTablet option
		navigation : true,
		pagination: false

	});
}

$('.format__block li a').click(function(e) {
    $('.format__block li').removeClass('active');
    $(this).parents('li').addClass('active');
    
    var index = $(this).attr('data-index');    
    var select = $('#select__format').find('[value=' + index + ']').text();
    
    $("#select__format-styler").find('.jq-selectbox__select-text').text(select);
    $("#select__format-styler").find('.selected.sel').removeClass('selected sel');
    $("#select__format-styler").find('li:contains(' + select + ')').addClass('selected sel');
    
    $("#select__format option[value=" + index + "]").attr('selected', 'true').text(select);
    $("#select__format").styler();
    e.preventDefault();
});

$('.format__block li').click(function(e) {
    $('.format__block li').removeClass('active');
    $(this).addClass('active');
    
    var index = $(this).find('a').attr('data-index');    
    var select = $('#select__format').find('[value=' + index + ']').text();
    
    $("#select__format-styler").find('.jq-selectbox__select-text').text(select);
    $("#select__format-styler").find('.selected.sel').removeClass('selected sel');
    $("#select__format-styler").find('li:contains(' + select + ')').addClass('selected sel');
    
    $("#select__format option[value=" + index + "]").attr('selected', 'true').text(select);
    $("#select__format").styler();
    e.preventDefault();
});

$("#select__format-styler li").click(function() {
    var select = $(this).text();
    var index = $('#select__format').find('option:contains(' + select + ')').val();
    
    var a = $('.format__block').find('[data-index=' + index + ']');
    $('.format__block li').removeClass('active');
    a.parents('li').addClass('active');
});

$('input, select').styler();

var calendar__item = $('.calendar__item-ul ul');
calendar__item.liBlockSize({
    type: 'max', //Тип выравнивания [max/min]
    side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
    child: 'p' //Эл-т, который выравниваем [child: '.selector']
});

var calendar__item = $('.calendar__item-ul ul');
calendar__item.liBlockSize({
    type: 'max', //Тип выравнивания [max/min]
    side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
    child: '.calendar__item-img + a' //Эл-т, который выравниваем [child: '.selector']
});

var calendar__item = $('.calendar__item-ul ul');
calendar__item.liBlockSize({
    type: 'max', //Тип выравнивания [max/min]
    side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
    child: '.p__date + a' //Эл-т, который выравниваем [child: '.selector']
});

var p_s = $('.popup__slider').bxSlider({
	pager: false,
	auto: true
});

$('.a__fancybox').fancybox({
	padding: 20
});

if (($('html').hasClass('mobile'))) {
	$('.fancybox').fancybox({
		padding: 0,
		autoSize: false,
		autoHeight: true,
		autoWidth: false,
		width: 625,
		afterLoad : function() {
			p_s.reloadSlider({
				pager: false
			});
		}
	});
} else {
	$('.fancybox').fancybox({
		padding: 0,
		autoSize: false,
		autoHeight: true,
		autoWidth: false,
		width: 980,
		afterLoad : function() {
			p_s.reloadSlider({
				pager: false,
				auto: true
			});
		}
	});
}



$('.fancybox__reg').fancybox({ // Для модальных окон с формами регистрации, авторизации, восстановления...
    padding: 0,
    autoSize: false,
    autoHeight: false,
    autoWidth: false,
    fitToView: false,
    scrolling: 'no',
    width: 560,
    height: 600,
    wrapCSS: "reg__fancy"
});
$('.fancybox__auth').fancybox({ // Для модальных окон с формами регистрации, авторизации, восстановления...
    padding: 0,
    autoSize: false,
    autoHeight: false,
    autoWidth: false,
    fitToView: false,
    scrolling: 'no',
    width: 560,
    height: 560,
    wrapCSS: "auth__fancy"
});
$('.fancybox__restore__openid').fancybox({ // Для модальных окон с формами регистрации, авторизации, восстановления...
    padding: 0,
    autoSize: false,
    autoHeight: false,
    autoWidth: false,
    fitToView: false,
    scrolling: 'no',
    width: 560,
    height: 300,
    wrapCSS: "restore__fancy"
});



$('.drop__menu').each(function() {
    var col = $(this).find('.menu__col').length;
    col = col * 207;
    $(this).width(col);
    
    var drop = $(this).find('.drop__menu');
    drop.liBlockSize({
        type: 'max', //Тип выравнивания [max/min]
        side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
        child: '.menu__col' //Эл-т, который выравниваем [child: '.selector']
    }); 
});

if (($('html').hasClass('mobile'))) {
	$('.menu .wrapper li').click(function() {
		if ($(this).hasClass('hover')) {
			$(this).removeClass('hover');
		} else {
			$(this).addClass('hover');
		}
	});
} else {
	$('.menu .wrapper li').mouseover(function() {
		$(this).addClass('hover');
		
		var drop = $(this).find('.drop__menu');
		drop.liBlockSize({
			type: 'max', //Тип выравнивания [max/min]
			side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
			child: '.menu__col' //Эл-т, который выравниваем [child: '.selector']
		});  
		var w_w = $(window).width();
		if (w_w < 1000) {
			w_w = 1000;
		}
		var left_body = (w_w - 980)/2;
		var w_li = $(this).offset().left - left_body;
		var w_li_r = 980 - w_li;
		var drop_w = $(this).find('.drop__menu').width();
		console.log(left_body + ' ' + w_li + ' ' + w_li_r);
		if (w_li_r < drop_w) {
			$(this).find('.drop__menu').addClass('to__right');
		}
	});

	$('.menu .wrapper li').mouseout(function() {
		$('.menu .wrapper li').removeClass('hover');
	});
}

if (($('html').hasClass('mobile'))) {
	$('.slider__block').each(function() {
		var block = $(this).find('.slider__block-ul');
		block.liBlockSize({
			type: 'max', //Тип выравнивания [max/min]
			side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
			child: '.slider__img + a' //Эл-т, который выравниваем [child: '.selector']
		}); 
	});
}

$('.calendar__item .item').each(function() {
	var block = $(this).find('.calendar__item-ul > div');
	block.liBlockSize({
		type: 'max', //Тип выравнивания [max/min]
		side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
		child: 'p' //Эл-т, который выравниваем [child: '.selector']
	}); 
});

$('.calendar__list ul li').click(function() {
	$('.calendar__list ul li').removeClass('active');
	$('.calendar__item .item').removeClass('active');
	var class_li = $(this).attr('class');
	$(this).addClass('active');
	
	$('.calendar__item').find('.' + class_li).addClass('active');
	$('.calendar__item .item').each(function() {
		var block = $(this).find('.calendar__item-ul > div');
		block.liBlockSize({
			type: 'max', //Тип выравнивания [max/min]
			side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
			child: 'p' //Эл-т, который выравниваем [child: '.selector']
		}); 
	});
});

$('.calendar__list-select select').change(function() {
	var class_li = $(this).val();
	$('.calendar__item .item').removeClass('active');
	$('.calendar__item').find('.' + class_li).addClass('active');
	$('.calendar__item .item').each(function() {
		var block = $(this).find('.calendar__item-ul > div');
		block.liBlockSize({
			type: 'max', //Тип выравнивания [max/min]
			side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
			child: 'p' //Эл-т, который выравниваем [child: '.selector']
		}); 
	});
});

$('.calendar__list-select li').click(function() {
	$('.calendar__list-select li').removeClass('selected sel');
	$('.calendar__item .item').removeClass('active');
	
	
	var class_li = 'item' + $(this).index();
	if (class_li == 'item0') {
		class_li = 'item1';
	}
	$(this).addClass('selected sel');
	
	$('.calendar__item').find('.' + class_li).addClass('active');
	$('.calendar__item .item').each(function() {
		var block = $(this).find('.calendar__item-ul > div');
		block.liBlockSize({
			type: 'max', //Тип выравнивания [max/min]
			side: 'h', //Сторона выравнивания [h(высота)/w(ширина)/wh]
			child: 'p' //Эл-т, который выравниваем [child: '.selector']
		}); 
	});
});

$('.jq-selectbox__select-text').click(function() {
	var pos = $(this).parents('.jq-selectbox').find('.jq-selectbox__dropdown').css('top');
	if (pos == 'auto') {
		$(this).parents('.jq-selectbox').addClass('bottom');
	} else {
		$(this).parents('.jq-selectbox').removeClass('bottom');
	}
});

$('.drop__block h2').click(function() {
	if ($(this).parent().hasClass('open')) {
		$(this).parent().removeClass('open');
	} else {
		$(this).parent().addClass('open');
	}
});

$.fn.scrollToTop=function() {
    $(this).hide().removeAttr("href");
    if($(window).scrollTop()!="0")
    {
        $(this).fadeIn("slow")
    }

    var scrollDiv=$(this);

    $(window).scroll(function()
    {
        if($(window).scrollTop()=="0")
        {
            $(scrollDiv).fadeOut("slow")
        }
        else
        {
            $(scrollDiv).fadeIn("slow")
        }
    });

    $("#up").click(function()
    {
      $("html, body").animate({scrollTop:0},"slow")
    })
}

$("#up").scrollToTop();

$('.menu__ico').click(function() {
	if ($('.menu').hasClass('open')) {
		$('.menu').removeClass('open');
	} else {
		if (($('html').hasClass('mobile')) && ($('html').hasClass('landscape'))) {
			var top_m = $('#top-cities-frame').offset().top + 73;
		} else {
			var top_m = $('#top-cities-frame').offset().top + 146;
		}
		$('.mobile .menu').css({'top': + top_m + 'px'});
		$('.menu').addClass('open');
	}
});

$('.table .table__title').click(function() {
	if ($(this).parents('.popup').length) {
		
	} else {
		$(this).parents('.table').find('.active').removeClass('active');
		$(this).parent().addClass('active');
	}
});

$('.mobile .hide__filter').click(function() {
	var display = $('.map__filter').css('display');
	
	if (display == 'block') {
		$('.map__filter').slideUp();
		$(this).text('Развернуть');
		$(this).addClass('hide');
	} else {
		$('.map__filter').slideDown();
		$(this).text('Свернуть');
		$(this).removeClass('hide');
	}
});

/** Отображение / Сокрытие списка выбора региона */
function showCitiesList(obj) {
    if($('#cities-list').css("display") === "none") {
        changeCitiesListHeight();
        $('#cities-list').slideDown(200,function(){
            changeCitiesListHeight();
            if(obj !== undefined) $(obj).addClass('city__up');
        });
    }
    else closeTopMenu();
}

/** Изменение высоты списка городов */
function changeCitiesListHeight() {
    $("#top-cities-frame").height($("#cities-list").height() + $(".top_circles").height() + 40);
}

/** Сокрытие списка выбора региона */
function closeTopMenu(){
    $('#cities-list').slideUp(200,function() {
        $("#top-cities-frame").height($(".top_circles").height() + 5);
        $('#city-active').children('a').removeClass('city__up');
    });
}

function setCity() {
    var city = '184';
    var link = document.getElementById('city_link_184');
    if (link) {
        var b = document.createElement('b');
            var text = link.innerHTML;
        b.innerHTML = text;
        link.parentNode.replaceChild(b,link);
        document.getElementById('city-active').innerHTML = '<a href="#" onclick="showCitiesList(this);return false;">'+text+'</a>';
    } else {                   
        if(city && typeof(city) != "undefined"){
            $.get("/russia/703/",
                  {"city_id":city},
                  function(data){
                      if(data && data.city_name)
                          document.getElementById('city-active').innerHTML = '<a href="#" onclick="showCitiesList(this);return false;">'+data.city_name+'</a>';
                  },
                  'json'); 
        }
        document.getElementById('city-active').innerHTML = '<a href="#" onclick="showCitiesList(this);return false;">Россия</a>';
    }

    if(typeof(city)=="undefined")
        $.get("/russia/703/",
              {"section_id":61},
              function(data){
                  $('body').append('<div id="s_all"></div>');
                  $('#s_all').html(data);
                  $('#s_overlay').css({'height':$('.main_table').height()+'px'});
                  $('#s_window').css({'top':($(window).height() - $('#s_window').height())/2+'px'});
                  setTimeout("$('#s_overlay').css({'height':$('.main_table').height()+'px'});", 1000);
              },
              'html');                	// указываем явно тип данных

}

/** Выбор городов по части имени (поисковому слову)
 * @param {string} name_part поисковое сово
 * @param {bool} from_begin если true - поисковое слово должно совпадать с началом названия города
 * @return {int} кол-во найденных городов */
function selectCitiesByPart(name_part, from_begin){
    var cities_li = [];
    var cities = [];
    for(var city_name in cities_list){
        var pos = city_name.toLowerCase().indexOf(name_part.toLowerCase());
        if(pos > -1 && !from_begin || from_begin && pos === 0)
            cities.push(city_name);
    }
    
    if(cities.length > 0){
        $("#no_city_match_error").hide();
        // Сортировать найденные города (чем ближе поисковое слово к началу города, тем оно левее)
        cities.sort(function(a,b){
            var a_pos = a.toLowerCase().indexOf(name_part.toLowerCase());
            var b_pos = b.toLowerCase().indexOf(name_part.toLowerCase())
            if(a_pos === b_pos)
                return a > b ? 1 : -1;
            return a_pos - b_pos;
        });
        for(var n in cities)
            cities_li.push(cities_list[cities[n]]["li"]);
    }
    else $("#no_city_match_error").show();
    $(".cities").html(cities_li.join(""));
    changeCitiesListHeight();
    return cities.length;
}

/** Очистка букв от выделения */
function clearLetters(){
    $(".letter").hide();
    $(".letter").find('.active').removeClass();
}

// Создание обработчиков событий для букв (городов)
$(".letters li a").click(function(){
    var letter = $(this).html();
    if(letter.length === 1){
        $(".letter").html(letter);
        clearLetters();
        $(".letter").show();
        $(this).addClass('active');
    }
    else clearLetters();
    selectCitiesByPart(letter, true);
});

// Создание обработчика событий для очистки поля поиска города
$("#clear_city").click(function(){
    $("#search_cities").val("");
    $("#search_cities").focus();
    clearLetters();
    selectCitiesByPart($("#search_cities").val());
});

// Создание обработчика события для изменения поля поиска города
$("#search_cities").keyup(function(e){
    clearLetters();
    var cities_match = selectCitiesByPart($("#search_cities").val());
    // Если нажат enter и найден 1 город - выбрать его
    if(e.keyCode === 13 && cities_match === 1){
        //alert(e.keyCode + " " + cities_match);
        location.href = $(".cities li a").attr("href");
    }
});

if ($('#map').length) {
	ymaps.ready(init);	
}

function init() {
	var myMap = new ymaps.Map('map', {
		center: [55.8, 37.6],
		zoom: 10
	}),
		
	// Создаем метку с помощью вспомогательного класса.
	myPlacemark1 = new ymaps.Placemark([55.8, 37.6], {
		// Свойства.
		// Содержимое иконки, балуна и хинта.
		iconContent: '1',
		balloonContent: '<div class="ballon__content"><div><a href="" class="a_h1">PICK POINT(Москва,Постамат: ТЦ 31)</a></div><h5>Адрес:</h5><p>Москва, Энтузиастов ш., д. 31, стр.39</p><div class="adress__info"><a href="images/content/little_child.jpg" class="img a__fancybox"><img src="images/content/img4_1.jpg" alt=""></a><h5>Описание:</h5><p>Постамат находится в ТЦ 31. Пешком: м. Шоссе один выход в город, из первых стеклянных по переходу до конца, затем повернуть направо</p></div><h5>Метро</h5><p>Шоссе Энтузиастов</p><h5>Время работы:</h5><p class="mobile__hide">Пн - 10:00-20:00, Вт - 10:00-20:00, Ср - 10:00-20:00, Чт - 10:00-20:00, Пт - 10:00-20:00, Сб - 10:00-20:00, Вс - 10:00-20:00</p><p class="mobile__display">Пн - Пт 10:00-20:00</p><div class="adress__time"><p class="left">Стоимость доставки: 215 руб</p><p class="right">Время доставки: 1 день</p></div><div class="adress__icon"><a href="" class="left favorites">В избранное</a><a href="" class="zoom">Приблизить</a><a href="" class="right">Подробнее</a></div></div>',
		hintContent: 'Стандартный значок метки'
	}, {
		// Опции.
		// Стандартная фиолетовая иконка.
		preset: 'twirl#violetIcon'
	});
	
	// Добавляем все метки на карту.
    myMap.geoObjects
    .add(myPlacemark1)
}

// Tabs for TXT page

$(document).on("click", ".tabs .tabs_menu li", function (e) {
    e.stopPropagation();
    e.preventDefault();
    var that = $(this);
    var dataTabID = that.find("a").data("tab");
    var selectedTabBlock = $("#" + dataTabID);
    that.closest(".txt__container").find(".this_tab").fadeOut(400);
    that.closest(".tabs_menu").find("li").removeClass("active");
    setTimeout(function () {
        selectedTabBlock.fadeIn(400);
    }, 401);

    that.addClass("active");
});

$(document).on("click", ".tabs .tabs_menu li a", function (e) {
    e.preventDefault();
});

$(document).ready(function () {	
	$(window).resize(function () {
		if ($('html').hasClass('mobile')) {
			if ($('#header__slider').length) {
				header__slider.reloadSlider({
					mode: 'fade',
					controls: true,
					auto: false,
					pager: false
				});
				$('#header__slider').parents('.bx-wrapper').addClass('header__sliders');
			}						
			
			var slider__block = $(".calendar__list ul");
			slider__block.owlCarousel({

				items : 2, //10 items above 1000px browser width
				itemsDesktop : [1000,2], //5 items between 1000px and 901px
				itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
				itemsTablet: [800,2], //2 items between 600 and 0;
				itemsMobile : [670,2], // itemsMobile disabled - inherit from itemsTablet option
				navigation : true,
				pagination: false

			});
		} else {
			if ($('#header__slider').length) {
				header__slider.reloadSlider({
					mode: 'fade',
					controls: false,
					auto: true
				});
				$('#header__slider').parents('.bx-wrapper').addClass('header__sliders');
			}
		}
	});
	
	var tempScrollTop, currentScrollTop = 0;
	
	$(window).scroll(function () {
		
		if (($('html').hasClass('mobile')) && ($('html').hasClass('landscape'))) {
			var currentScrollTop = $('#top-cities-frame').offset().top + 73;
		} else {
			var currentScrollTop = $('#top-cities-frame').offset().top + 146;
		}
		if (tempScrollTop < currentScrollTop ) {
			
		}			
		else if (tempScrollTop > currentScrollTop ) {
			$('.mobile .menu').css({'top': + currentScrollTop + 'px'});
		}
		//scrolling up

		tempScrollTop = currentScrollTop;
	});
});