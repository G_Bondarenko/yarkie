<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="shop__book" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <?php include('parts/photo__block11.php'); ?>
            <?php include('parts/calendar__item5.php'); ?>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>