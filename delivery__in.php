<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="delivery__in" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <div class="delivery__in">
				<div class="wrapper">
					<h1>Все способы доставки</h1>	
				</div>
				<?php include('parts/delivery__punkt.php'); ?>
				<?php include('parts/delivery__terminal.php'); ?>
			</div>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>