<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="service" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <?php include('parts/service__header.php'); ?>
            <?php include('parts/service__list.php'); ?>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>