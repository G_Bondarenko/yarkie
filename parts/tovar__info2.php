<div class="tovar__info">
    <div class="wrapper clearfix">
        <div class="img__desc">
           <div class="tovar__slider__wrap">
                <ul class="tovar__slider">
                    <li><img src="images/slider/img7_1.jpg" /></li>
                    <li><img src="images/template/clear__photo.jpg" /></li>
                    <li><img src="images/template/clear__photo.jpg" /></li>
                    <li><img src="images/template/clear__photo.jpg" /></li>
                </ul>

                <div id="tovar__slider-pager" class="tovar__slider-pager">
                    <a data-slide-index="0" href=""><img src="images/template/clear__photo.jpg" /></a>
                    <a data-slide-index="1" href=""><img src="images/template/clear__photo.jpg" /></a>
                    <a data-slide-index="2" href=""><img src="images/template/clear__photo.jpg" /></a>
                    <a data-slide-index="3" href=""><img src="images/template/clear__photo.jpg" /></a>
                </div>
            </div>
            <h5>
                Картина с паспарту создана для любителей портретов
                и домашних фотокартин в духе XX-го столетия.
            </h5>
            <p>
                Тогда было модно дарить знакомым фотокарточки с памятной
                надписью или автографом на паспарту. Мы используем современные
                материалы и технологии — картина с паспарту прослужит вам
                не одно десятилетие, и при этом останется как и прежде личным
                и по-дружески тёплым подарком.
            </p>
        </div>
        <div class="tovar__detail">
            <h1>Название выбранного товара</h1>
            <div class="tovar__detail-price">
                <dt>от</dt> <span class="new__price">3 200 </span> <span class="old__price">3 600 P</span>
            </div>
            
            <h5>Выберите формат</h5>
            <select id="select__format" name="select__format" >
                <option value="0">20x20</option>
                <option value="1">30x30</option>
                <option value="2">40x40</option>
                <option value="3">50x50</option>
                <option value="4">60x60</option>
                <option value="5">60x60</option>
                <option value="6">70x70</option>
                <option value="7">80x80</option>
                <option value="8">90x90</option>
                <option value="9">100x100</option>
            </select>
            
            <div><a href="#popup" class="fancybox a__butt a__submit">ОФОРМИТЬ ТОВАР</a></div>
            
            <ul class="specifications">                
                <li class="alt">
                    <span class="left">Разрешение печати:</span>
                    <span class="right">200 dpi</span>
                </li>
                <li>
                    <span class="left">Материал:</span>
                    <span class="right">пенокартон + картон + акрил</span>
                </li>
                <li class="alt">
                    <span class="left">Упаковка:</span>
                    <span class="right">гофрокартон с пузырчатой плёнкой</span>
                </li>
                <li>
                    <span class="left">Особенности:</span>
                    <span class="right">багет с подвесом</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="wrapper">
    	<div class="mobile__display drop__block">
        	<h2>
        		Описание
        	</h2>
			<div class="drop__block-inner">
				<h5>
					Картина с паспарту создана для любителей портретов
                	и домашних фотокартин в духе XX-го столетия.
				</h5>
				<p>
					Тогда было модно дарить знакомым фотокарточки с памятной
					надписью или автографом на паспарту. Мы используем современные
					материалы и технологии — картина с паспарту прослужит вам
					не одно десятилетие, и при этом останется как и прежде личным
					и по-дружески тёплым подарком.
				</p>
			</div>            
        </div>
        <div class="mobile__display drop__block specifications">
        	<h2>Характеристики</h2>
        	<div class="drop__block-inner">
				<ul>
					<li class="alt">
                    <span class="left">Разрешение печати:</span>
                    <span class="right">200 dpi</span>
                </li>
                <li>
                    <span class="left">Материал:</span>
                    <span class="right">пенокартон + картон + акрил</span>
                </li>
                <li class="alt">
                    <span class="left">Упаковка:</span>
                    <span class="right">гофрокартон с пузырчатой плёнкой</span>
                </li>
                <li>
                    <span class="left">Особенности:</span>
                    <span class="right">багет с подвесом</span>
                </li>
				</ul>
			</div>
        </div>
    </div>
</div>