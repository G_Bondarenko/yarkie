<div class="calendar__list without mobile__hide">
    <div class="wrapper">
        <ul>
            <li class="item1">
                <div class="calendar__icon icon1"></div>
                <a href="#">Настольные</a>
            </li>
            <li class="item2">
                <div class="calendar__icon icon2"></div>
                <a href="#">Малые</a>
            </li>
            <li class="item3">
                <div class="calendar__icon icon3"></div>
                <a href="#">Большие</a>
            </li>
            <li class="item4 active">
                <div class="calendar__icon icon4"></div>
                <a href="#">Средние</a>
            </li>
            <li class="item5">
                <div class="calendar__icon icon5"></div>
                <a href="#">Одностраничные</a>
            </li>
        </ul>
    </div>
</div>
<div class="mobile__display calendar__list-select">
	<div class="wrapper">
		<select>
			<option value="0">Выбрать размер</option>
			<option value="item1">Настольные</option>
			<option value="item2">Малые</option>
			<option value="item3">Большие</option>
			<option value="item4">Средние</option>
			<option value="item5">Одностраничные</option>
		</select>
	</div>
</div>