<div class="txt__container">

    <div class="txt__tabs tabs">
        <ul class="tabs_menu wrapper">
            <li class="active">
                <a href="#" data-tab="tab1">Название меню</a>
            </li>
            <li>
                <a href="#" data-tab="tab2">Название меню</a>

            </li>
            <li>
                <a href="#" data-tab="tab3">Название меню</a>

            </li>
            <li>
                <a href="#" data-tab="tab4">Название меню</a>

            </li>
            <li>
                <a href="#" data-tab="tab5">Название меню</a>

            </li>
        </ul>
    </div>
    <div class="wrapper clearfix">
        <div id="tab1" class="txt__content this_tab">
            <h1>Заголовок для страницы</h1>

            <p>
                Если вы выбрали стиль из бесплатной коллекции <a href="#">netPrint.ru</a> – страницы макета уже будут художественно оформлены, вам достаточно
                добавить фотографии и редактировать тексты. Если макет создаётся с нуля, в онлайн-редакторе вы увидите чистые страницы,
                как в нашем примере.
            </p>

            <div class="pic__text__wrap clearfix">
                <div class="left__pic left clearfix">
                    <img src="images/content/little_child.jpg" alt=""/>
                </div>
                <div class="right__text">
                    <h5>Еще один заголовок для этого текста с картинкой</h5>
                    <p>
                        Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                        Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                    </p>

                    <ul class="long__defis__list">
                        <li>Название из списка</li>
                        <li>Еще одно название</li>
                        <li>Третье название</li>
                    </ul>
                    <a class="a__butt" href="#">Подробнее</a>
                </div>
            </div>

            <div class="table__block clear__both">
                <h5>Еще один заголовок для этого текста с картинкой</h5>
                <p>
                    Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                    Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                </p>

                <table width="90%">
                    <tbody>
                    <tr class="thead">
                        <th class="format" rowspan="2">Формат</th>
                        <th class="do" colspan="2">Дообрезной размер (мм.)</th>
                        <th class="post" colspan="2">Послеобрезной размер (мм.)</th>
                        <th class="kor">Корешок (мм.)</th>
                    </tr>
                    <tr>
                        <td class="do1 th">Развороты</td>
                        <td class="do2 th">Обложки</td>
                        <td class="post1 th">Развороты</td>
                        <td class="post2 th">Обложки</td>
                        <td class="kor th">по числу разворотов</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х30</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">40х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr class="alt">
                        <td class="format">40х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">20х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="most__important__info">
                <span>Выделение текстового блока для особо важной информации в две строки</span>
            </div>

            <h5>Ещё один заголовок</h5>
            <p>
                Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
            </p>

            <div class="grey__info">
                <span>
                    Еще один способ выделения текстового блока для этого сайта на этой странице <br>
                    <a href="#">форму регистрации для партнеров</a>
                </span>
            </div>

            <a class="a__butt" href="#">Подробнее</a>

        </div>

        <div id="tab2" class="txt__content this_tab hidden">
            <h1>Заголовок для страницы</h1>

            <p>
                Если вы выбрали стиль из бесплатной коллекции <a href="#">netPrint.ru</a> – страницы макета уже будут художественно оформлены, вам достаточно
                добавить фотографии и редактировать тексты. Если макет создаётся с нуля, в онлайн-редакторе вы увидите чистые страницы,
                как в нашем примере.
            </p>

            <div class="pic__text__wrap clearfix">
                <div class="left__pic left clearfix">
                    <img src="images/content/little_child.jpg" alt=""/>
                </div>
                <div class="right__text">
                    <h5>Еще один заголовок для этого текста с картинкой</h5>
                    <p>
                        Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                        Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                    </p>

                    <ul class="long__defis__list">
                        <li>Название из списка</li>
                        <li>Еще одно название</li>
                        <li>Третье название</li>
                    </ul>
                    <a class="a__butt" href="#">Подробнее</a>
                </div>
            </div>

            <div class="table__block clear__both">
                <h5>Еще один заголовок для этого текста с картинкой</h5>
                <p>
                    Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                    Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                </p>

                <table width="90%">
                    <tbody>
                    <tr class="thead">
                        <th class="format" rowspan="2">Формат</th>
                        <th class="do" colspan="2">Дообрезной размер (мм.)</th>
                        <th class="post" colspan="2">Послеобрезной размер (мм.)</th>
                        <th class="kor">Корешок (мм.)</th>
                    </tr>
                    <tr>
                        <td class="do1 th">Развороты</td>
                        <td class="do2 th">Обложки</td>
                        <td class="post1 th">Развороты</td>
                        <td class="post2 th">Обложки</td>
                        <td class="kor th">по числу разворотов</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х30</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">40х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr class="alt">
                        <td class="format">40х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">20х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="most__important__info">
                <span>Выделение текстового блока для особо важной информации в две строки</span>
            </div>

            <h5>Ещё один заголовок</h5>
            <p>
                Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
            </p>

            <div class="grey__info">
                <span>
                    Еще один способ выделения текстового блока для этого сайта на этой странице <br>
                    <a href="#">форму регистрации для партнеров</a>
                </span>
            </div>

            <a class="a__butt" href="#">Подробнее</a>

        </div>

        <div id="tab3" class="txt__content this_tab hidden">
            <h1>Заголовок для страницы</h1>

            <p>
                Если вы выбрали стиль из бесплатной коллекции <a href="#">netPrint.ru</a> – страницы макета уже будут художественно оформлены, вам достаточно
                добавить фотографии и редактировать тексты. Если макет создаётся с нуля, в онлайн-редакторе вы увидите чистые страницы,
                как в нашем примере.
            </p>

            <div class="pic__text__wrap clearfix">
                <div class="left__pic left clearfix">
                    <img src="images/content/little_child.jpg" alt=""/>
                </div>
                <div class="right__text">
                    <h5>Еще один заголовок для этого текста с картинкой</h5>
                    <p>
                        Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                        Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                    </p>

                    <ul class="long__defis__list">
                        <li>Название из списка</li>
                        <li>Еще одно название</li>
                        <li>Третье название</li>
                    </ul>
                    <a class="a__butt" href="#">Подробнее</a>
                </div>
            </div>

            <div class="table__block clear__both">
                <h5>Еще один заголовок для этого текста с картинкой</h5>
                <p>
                    Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                    Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                </p>

                <table width="90%">
                    <tbody>
                    <tr class="thead">
                        <th class="format" rowspan="2">Формат</th>
                        <th class="do" colspan="2">Дообрезной размер (мм.)</th>
                        <th class="post" colspan="2">Послеобрезной размер (мм.)</th>
                        <th class="kor">Корешок (мм.)</th>
                    </tr>
                    <tr>
                        <td class="do1 th">Развороты</td>
                        <td class="do2 th">Обложки</td>
                        <td class="post1 th">Развороты</td>
                        <td class="post2 th">Обложки</td>
                        <td class="kor th">по числу разворотов</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х30</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">40х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr class="alt">
                        <td class="format">40х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">20х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="most__important__info">
                <span>Выделение текстового блока для особо важной информации в две строки</span>
            </div>

            <h5>Ещё один заголовок</h5>
            <p>
                Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
            </p>

            <div class="grey__info">
                <span>
                    Еще один способ выделения текстового блока для этого сайта на этой странице <br>
                    <a href="#">форму регистрации для партнеров</a>
                </span>
            </div>

            <a class="a__butt" href="#">Подробнее</a>

        </div>

        <div id="tab4" class="txt__content this_tab hidden">
            <h1>Заголовок для страницы</h1>

            <p>
                Если вы выбрали стиль из бесплатной коллекции <a href="#">netPrint.ru</a> – страницы макета уже будут художественно оформлены, вам достаточно
                добавить фотографии и редактировать тексты. Если макет создаётся с нуля, в онлайн-редакторе вы увидите чистые страницы,
                как в нашем примере.
            </p>

            <div class="pic__text__wrap clearfix">
                <div class="left__pic left clearfix">
                    <img src="images/content/little_child.jpg" alt=""/>
                </div>
                <div class="right__text">
                    <h5>Еще один заголовок для этого текста с картинкой</h5>
                    <p>
                        Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                        Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                    </p>

                    <ul class="long__defis__list">
                        <li>Название из списка</li>
                        <li>Еще одно название</li>
                        <li>Третье название</li>
                    </ul>
                    <a class="a__butt" href="#">Подробнее</a>
                </div>
            </div>

            <div class="table__block clear__both">
                <h5>Еще один заголовок для этого текста с картинкой</h5>
                <p>
                    Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                    Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                </p>

                <table width="90%">
                    <tbody>
                    <tr class="thead">
                        <th class="format" rowspan="2">Формат</th>
                        <th class="do" colspan="2">Дообрезной размер (мм.)</th>
                        <th class="post" colspan="2">Послеобрезной размер (мм.)</th>
                        <th class="kor">Корешок (мм.)</th>
                    </tr>
                    <tr>
                        <td class="do1 th">Развороты</td>
                        <td class="do2 th">Обложки</td>
                        <td class="post1 th">Развороты</td>
                        <td class="post2 th">Обложки</td>
                        <td class="kor th">по числу разворотов</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х30</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">40х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr class="alt">
                        <td class="format">40х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">20х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="most__important__info">
                <span>Выделение текстового блока для особо важной информации в две строки</span>
            </div>

            <h5>Ещё один заголовок</h5>
            <p>
                Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
            </p>

            <div class="grey__info">
                <span>
                    Еще один способ выделения текстового блока для этого сайта на этой странице <br>
                    <a href="#">форму регистрации для партнеров</a>
                </span>
            </div>

            <a class="a__butt" href="#">Подробнее</a>

        </div>

        <div id="tab5" class="txt__content this_tab hidden">
            <h1>Заголовок для страницы</h1>

            <p>
                Если вы выбрали стиль из бесплатной коллекции <a href="#">netPrint.ru</a> – страницы макета уже будут художественно оформлены, вам достаточно
                добавить фотографии и редактировать тексты. Если макет создаётся с нуля, в онлайн-редакторе вы увидите чистые страницы,
                как в нашем примере.
            </p>

            <div class="pic__text__wrap clearfix">
                <div class="left__pic left clearfix">
                    <img src="images/content/little_child.jpg" alt=""/>
                </div>
                <div class="right__text">
                    <h5>Еще один заголовок для этого текста с картинкой</h5>
                    <p>
                        Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                        Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                    </p>

                    <ul class="long__defis__list">
                        <li>Название из списка</li>
                        <li>Еще одно название</li>
                        <li>Третье название</li>
                    </ul>
                    <a class="a__butt" href="#">Подробнее</a>
                </div>
            </div>

            <div class="table__block clear__both">
                <h5>Еще один заголовок для этого текста с картинкой</h5>
                <p>
                    Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                    Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
                </p>

                <table width="90%">
                    <tbody>
                    <tr class="thead">
                        <th class="format" rowspan="2">Формат</th>
                        <th class="do" colspan="2">Дообрезной размер (мм.)</th>
                        <th class="post" colspan="2">Послеобрезной размер (мм.)</th>
                        <th class="kor">Корешок (мм.)</th>
                    </tr>
                    <tr>
                        <td class="do1 th">Развороты</td>
                        <td class="do2 th">Обложки</td>
                        <td class="post1 th">Развороты</td>
                        <td class="post2 th">Обложки</td>
                        <td class="kor th">по числу разворотов</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х30</td>
                        <td class="do1">400х203</td>
                        <td class="do2">476.4х240</td>
                        <td class="post1">400х203</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">40х20</td>
                        <td class="do1">400х270</td>
                        <td class="do2"></td>
                        <td class="post1">400х270</td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr class="alt">
                        <td class="format">20х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr>
                        <td class="format">30х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2">400х270</td>
                        <td class="kor"></td>
                    </tr>
                    <tr class="alt">
                        <td class="format">40х20</td>
                        <td class="do1">400х203</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor">14.4-32.4</td>
                    </tr>
                    <tr>
                        <td class="format">20х20</td>
                        <td class="do1">600х305</td>
                        <td class="do2"></td>
                        <td class="post1"></td>
                        <td class="post2"></td>
                        <td class="kor"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="most__important__info">
                <span>Выделение текстового блока для особо важной информации в две строки</span>
            </div>

            <h5>Ещё один заголовок</h5>
            <p>
                Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни... Вы ведь тоже любите фотографировать?
                Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних приключений и получайте на них скидки!
            </p>

            <div class="grey__info">
                <span>
                    Еще один способ выделения текстового блока для этого сайта на этой странице <br>
                    <a href="#">форму регистрации для партнеров</a>
                </span>
            </div>

            <a class="a__butt" href="#">Подробнее</a>

        </div>

    </div>
</div>