<div class="service__list">
	<ul>
		<li>
			<div class="wrapper">
				<div class="clearfix">
					<div class="img">
						<img src="images/content/img5_1.jpg" alt="">
					</div>
					<div class="service__list-info">
						<h3><a href="#">Партнерская программа</a></h3>
						<p>
							Присоединяйтесь к нашей партнерской программе и зарабатывайте с нами!
						</p>
						<ul class="list">
							<li>Разместите на вашем сайте ссылку, ведущую на netPrint.ru.</li>
							<li>
								Получайте 5% от стоимости каждого заказа, оформленного клиентами, которые<br>
								перешли по партнерской ссылке на наш сайт.
							</li>
						</ul>
						<div class="block__detail">
							<p>
								Для участия в партнерской программе авторизуйтесь на сайте netPrint.ru, и заполните
								<a href="">форму регистрации <span class="mobile__hide">для партнеров</span></a>
							</p>
						</div>
						<div>
							<a href="#" class="a__butt">ПОДРОБНЕЕ</a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="alt">
			<div class="wrapper">
				<div class="clearfix">
					<div class="img">
						<img src="images/content/img5_2.jpg" alt="">
					</div>
					<div class="service__list-info">
						<h3><a href="#">Добавляйте фотографии из социальных сетей</a></h3>
						<p>
							Отличная возможность для тех, кто любит сидеть сутки напролет Вконтакте, Facebook<br>
							и в Mail.ru. Печатайте любимые фотосувениры и книги, загружая фотографии<br>
							из социальных сетей
						</p>
						<ul class="list">
							<li>Выбирайте оригинальные персональные фотоподарки для своих близких и друзей.</li>
							<li>
								Загружайте фотографии в шаблоны сувениров и книг прямо из альбомов Вконтакте,<br>
								Facebook и Mail.ru
							</li>
							<li>
								Оформляйте подарки любимым в любом месте и в любое время, не выходя<br>
								из соцсетей.
							</li>
						</ul>
						<div class="block__detail white">
							<p>
								Оформляйте сувениры и книги с фотографиями из социальных сетей
							</p>
						</div>
						<div>
							<a href="#" class="a__butt">ПОДРОБНЕЕ</a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="wrapper">
				<div class="clearfix">
					<div class="img">
						<img src="images/content/img5_2.jpg" alt="">
					</div>
					<div class="service__list-info">
						<h3><a href="#">Ваш личный фотоархив</a></h3>
						<p>
							Предоставляем нашим пользователям неограниченное пространство на сервере<br>
							для загрузки фотографий. Вы сами выбираете уровень доступа к своим фотоальбомам:<br>
							личный, общий или «для друзей».
						</p>
						<p>
							Фотографии в Online-альбомах, даже если ими никто не пользуется, будут храниться<br>
							на сайте 2 года с момента оформления последнего заказа.
						</p>
						<ul class="list">
							<li><a href="">Альбомы для друзей</a>. Откройте друзьям доступ к своим альбомам!</li>
							<li>
								<a href="">Фотогалереи</a>. Ваша личная фотовыставка.
							</li>
						</ul>
						<div>
							<a href="#" class="a__butt">ПОДРОБНЕЕ</a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="alt">
			<div class="wrapper">
				<div class="clearfix">
					<div class="img">
						<img src="images/content/img5_4.jpg" alt="">
					</div>
					<div class="service__list-info">
						<h3><a href="#">Мобильные приложения</a></h3>
						<p>
							Новинка: netPrint.ru в вашем iPhone или Android! Простое и удобное приложение<br>
							позволяет:
						</p>
						<ul class="list">
							<li>Загружать фотографии с вашего телефона прямиком в фотоальбомы вашего аккаунта на netPrint.r</li>
							<li>
								Получать уведомления о новых акциях и предложениях
							</li>
							<li>
								Просматривать свои фотографии и фотографии друзей в режиме слайд-шоу
							</li>
						</ul>
						<div class="block__detail white">
							<p>
								Приложение можно бесплатно скачать:
							</p>
							<a href="#" class="a__app"><img src="images/template/app.png" alt=""></a>
							<a href="#" class="a__google"><img src="images/template/google.png" alt=""></a>
						</div>
						<div>
							<a href="#" class="a__butt">ПОДРОБНЕЕ</a>
						</div>
					</div>
				</div>
			</div>
		</li>
	</ul>
</div>