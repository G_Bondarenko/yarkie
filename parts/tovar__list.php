<div class="tovar__list">
    <div class="wrapper">
        <h1>
            Принтбук ROYAL в твердой персональной обложке 25х25,
            10 разворотов
        </h1>
    </div>
    <div class="tovar__list-ul">
        <div>
            <ul class="wrapper">
                <li>
                    <div class="tovar__list-img">
						<a href=""><img src="images/slider/img9_1.jpg" alt=""></a>
                    </div>
                    <p>
                        Принтбук ROYAL в твердой
                        персональной обложке 25х25,
                        стиль "Европа"
                    </p>
                    <a href="#" class="a__butt">СОЗДАТЬ</a>
                </li>
                <li>
                    <div class="tovar__list-img">
						<a href=""><img src="images/slider/img9_2.jpg" alt=""></a>
                    </div>
                    <p>
                        Принтбук ROYAL в твердой
                        персональной обложке 25х25,
                        стиль "Азия"
                    </p>
                    <a href="#" class="a__butt">СОЗДАТЬ</a>
                </li>
                <li>
                    <div class="tovar__list-img">
						<a href=""><img src="images/slider/img9_3.jpg" alt=""></a>
                    </div>
                    <p>
                        Принтбук ROYAL в твердой
                        персональной обложке 25х25,
                        стиль "Итоги года"
                    </p>
                    <a href="#" class="a__butt">СОЗДАТЬ</a>
                </li>
            </ul>
        </div>
        <div class="alt">
            <ul class="wrapper">
                <li>
                    <div class="tovar__list-img">
						<a href=""><img src="images/slider/img9_4.jpg" alt=""></a>
                    </div>
                    <p>
                        Принтбук ROYAL в твердой
                        персональной обложке 25х25,
                        стиль "Путешествия. Африка."
                    </p>
                    <a href="#" class="a__butt">СОЗДАТЬ</a>
                </li>
                <li>
                    <div class="tovar__list-img">
						<a href=""><img src="images/slider/img9_5.jpg" alt=""></a>
                    </div>
                    <p>
                        Принтбук ROYAL в твердой
                        персональной обложке 25х25,
                        стиль "Спортивное лето"
                    </p>
                    <a href="#" class="a__butt">СОЗДАТЬ</a>
                </li>
                <li>
                    <div class="tovar__list-img">
						<a href=""><img src="images/slider/img9_6.jpg" alt=""></a>
                    </div>
                    <p>
                        Принтбук ROYAL в твердой
                        персональной обложке 25х25,
                        стиль "Нежность"
                    </p>
                    <a href="#" class="a__butt">СОЗДАТЬ</a>
                </li>
            </ul>
        </div>
    </div>
</div>