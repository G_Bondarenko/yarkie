<div class="article__block block_iphone">
    <img src="images/bg/img2.jpg" class="slider__bg" alt="">
    <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Печатай фотографии<br> и декор прямо со своего телефона</h2>
                <div>
                    <a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
                </div>
            </div>
        </div>
    </div>
</div>