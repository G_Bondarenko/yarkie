<div class="quality__block mobile__hide">
    <div class="wrapper">
        <h2>КАЧЕСТВО В КАЖДОЙ ДЕТАЛИ</h2>
        <ul>
            <li>
               <div class="vertical__align">
                    <p>
                        Аккуратная биговка незаметна на развороте<br>
                        и не портит фото.
                    </p>
                </div>
            </li>
            <li>
                <div class="vertical__align">
                    <p>
                        Фотобумага Шёлк – новый вид бумаги с текстурой<br>
                        шёлковой ткани.
                    </p>
                </div>
            </li>
            <li>
                <div class="vertical__align">
                    <p>
                        Страницы усилены специальной прослойкой <br>
                        из картона, что делает листы очень плотными,<br>
                        но гибкими, как пластик.
                    </p>
                </div>
            </li>
            <li>
                <div class="vertical__align">
                    <p>
                        Принтбуки 40х30 укреплены капталом – х/б лентой<br>
                        по краю корешка книги.
                    </p>
                </div>
            </li>
            <li>
               <div class="vertical__align">
                    <p>
                        Фотобумага Металлик отличается металлическим<br>
                        блеском с жемчужным оттенком.
                    </p>
                </div>
            </li>
            <li>
               <div class="vertical__align">
                    <p>
                        30 разворотов (60 страниц) – максимальное количество<br>
                        для формата ROYAL.
                    </p>
                </div>
            </li>
        </ul>
    </div>
</div>