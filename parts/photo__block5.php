<div class="article__block article__block-photo article__souvenir">
  <img src="images/bg/img5.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Фотосувениры</h2>
                <p class="mobile__hide">
                    Украшайте каждую страницу календаря вашими любимыми
                    фотографиями, выбирайте дизайнерские стили оформления,
                    начинайте календарь с любого месяца и добавляйте фото
                    и подписи в сетку календаря (опция событий).
                </p>
                <p class="mobile__display">
                	«Маленькие персональные радости»
                </p>
                <div>
                    <a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
                </div>
            </div>
        </div>
    </div>
</div>