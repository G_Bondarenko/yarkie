<div class="quality__block mobile__hide">
    <div class="wrapper">
        <h2>КАЧЕСТВО В КАЖДОЙ ДЕТАЛИ</h2>
        <ul>
            <li>
               <div class="vertical__align">
                    <p>
                        Основа – ваши любимые фотографии.
                    </p>
               </div>               
            </li>
            <li>
               <div class="vertical__align">
                    <p>
                        Большой выбор различных шаблонов.
                    </p>
               </div>               
            </li>
            <li>
               <div class="vertical__align">
                    <p>
                        Добавьте ваши личные надписи и пожелания.
                    </p>
               </div>               
            </li>
        </ul>
    </div>
</div>