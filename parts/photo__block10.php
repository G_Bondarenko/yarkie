<div class="article__block article__block-photo block__gal">
  <img src="images/bg/img10.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Фотосувениры</h2>
                <p>
                    Украшайте каждую страницу календаря вашими любимыми
					фотографиями, выбирайте дизайнерские стили оформления,
					начинайте календарь с любого месяца и добавляйте фото
					и подписи в сетку календаря (опция событий).
                </p>
            </div>
        </div>
    </div>
</div>