<div class="slider__block gray">
   <div class="wrapper">
        <h2>Спецпредложения</h2>
        <ul class="slider__block-ul">
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img2_1.jpg" alt=""></a>
                </div>
                <a href="#">Каскад скидок на интерьерную печать</a>
                <p>15% до 8 апреля</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img2_2.jpg" alt=""></a>
                </div>
                <a href="#">Скидка 50%</a>
                <p>На каждый второй сувени</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img2_3.jpg" alt=""></a>
                </div>
                <a href="#">Минибук</a>
                <p>20 разворотов 18*13</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                    <span class="old__price">885 P</span>
                </div>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img2_1.jpg" alt=""></a>
                </div>
                <a href="#">Каскад скидок на интерьерную печать</a>
                <p>15% до 8 апреля</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img2_2.jpg" alt=""></a>
                </div>
                <a href="#">Скидка 50%</a>
                <p>На каждый второй сувени</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img2_3.jpg" alt=""></a>
                </div>
                <a href="#">Минибук</a>
                <p>20 разворотов 18*13</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                    <span class="old__price">885 P</span>
                </div>
            </li>
        </ul>
    </div>
</div>