<div id="technical" class="popup">
	<div class="popup__inner">
		<h1 class="text__center">
			Технические размеры
		</h1>
		<p class="mobile__hide">
			При подготовке изображений в графических редакторах (например, Photoshop) пользуйтесь только ДООБРЕЗНЫМИ<br>
			размерами или <a href="">готовыми шаблонами</a>!
		</p>
		<p class="mobile__display">
			При подготовке изображений<br>
			в графических редакторах (например,<br>
			Photoshop) пользуйтесь только<br>
			ДООБРЕЗНЫМИ размерами или<br>
			<a href="">готовыми шаблонами</a>!
		</p>
		<table>
			<tbody>
				<tr class="thead">
					<th class="format" rowspan="2">Формат</th>
					<th class="do" colspan="2">Дообрезной размер (мм.)</th>
					<th class="post" colspan="2">Послеобрезной размер (мм.)</th>
					<th class="kor">Корешок (мм.)</th>
				</tr>
				<tr>
					<td class="do1 th">Развороты</td>
					<td class="do2 th">Обложки</td>
					<td class="post1 th">Развороты</td>
					<td class="post2 th">Обложки</td>
					<td class="kor th">по числу разворотов</td>
				</tr>
				<tr class="alt">
					<td class="format">20х20</td>
					<td class="do1">400х203</td>
					<td class="do2">476.4х240</td>
					<td class="post1">400х203</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr>
					<td class="format">30х20</td>
					<td class="do1">400х270</td>
					<td class="do2"></td>
					<td class="post1">400х270</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr class="alt">
					<td class="format">20х30</td>
					<td class="do1">400х203</td>
					<td class="do2">476.4х240</td>
					<td class="post1">400х203</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr>
					<td class="format">40х20</td>
					<td class="do1">400х270</td>
					<td class="do2"></td>
					<td class="post1">400х270</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr class="alt">
					<td class="format">20х20</td>
					<td class="do1">400х203</td>
					<td class="do2"></td>
					<td class="post1"></td>
					<td class="post2">400х270</td>
					<td class="kor"></td>
				</tr>
				<tr>
					<td class="format">30х20</td>
					<td class="do1">600х305</td>
					<td class="do2"></td>
					<td class="post1"></td>
					<td class="post2">400х270</td>
					<td class="kor"></td>
				</tr>
				<tr class="alt">
					<td class="format">40х20</td>
					<td class="do1">400х203</td>
					<td class="do2"></td>
					<td class="post1"></td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr>
					<td class="format">20х20</td>
					<td class="do1">600х305</td>
					<td class="do2"></td>
					<td class="post1"></td>
					<td class="post2"></td>
					<td class="kor"></td>
				</tr>
			</tbody>
		</table>
		<ul class="mobile__display table">
			<li>
				<div class="table__title">Формат 20x20</div>
				<div class="table__info">
					<div class="table__info__row alt">Дообрезной размер (мм.)</div>
					<ul>
						<li>Развороты</li>
						<li>Обложки</li>
						<li class="alt">400х203</li>
						<li class="alt">476.4х240</li>
					</ul>
				</div>
			</li>
			<li class="active">
				<div class="table__title">Формат 20x30</div>
				<div class="table__info">
					<div class="table__info__row alt">Дообрезной размер (мм.)</div>
					<ul>
						<li>Развороты</li>
						<li>Обложки</li>
						<li class="alt">400х203</li>
						<li class="alt">476.4х240</li>
					</ul>
				</div>
			</li>
			<li>
				<div class="table__title">Формат 20x40</div>
				<div class="table__info">
					<div class="table__info__row alt">Дообрезной размер (мм.)</div>
					<ul>
						<li>Развороты</li>
						<li>Обложки</li>
						<li class="alt">400х203</li>
						<li class="alt">476.4х240</li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</div>