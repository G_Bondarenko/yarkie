<div class="calendar__list">
    <div class="wrapper">
        <h2>Фотокалендари</h2>
        <ul>
            <li>
                <div class="calendar__icon icon1"></div>
                <a href="#">Настольные</a>
            </li>
            <li>
                <div class="calendar__icon icon2"></div>
                <a href="#">Малые</a>
            </li>
            <li>
                <div class="calendar__icon icon3"></div>
                <a href="#">Большие</a>
            </li>
            <li>
                <div class="calendar__icon icon4"></div>
                <a href="#">Средние</a>
            </li>
            <li>
                <div class="calendar__icon icon5"></div>
                <a href="#">Одностраничные</a>
            </li>
        </ul>
    </div>
</div>