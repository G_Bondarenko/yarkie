<div class="article__block article__block-photo block__gal">
  <img src="images/bg/img8.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Интерьерная печать</h2>
                <p>
                    Стиль и лаконичность - изысканное оформление интерьеров.
					Создавайте красивое пространство вокруг себя
                </p>
            </div>
        </div>
    </div>
</div>