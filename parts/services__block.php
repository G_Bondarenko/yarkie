<div class="slider__block">
   <div class="wrapper">
        <h2>Услуги и сервисы</h2>
        <ul class="slider__block-ul">
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img3_1.jpg" alt=""></a>
                </div>
                <a href="#">Мобильные приложения</a>
                <p>Новинка: netPrint.ru в вашем iPhone или Android!</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img3_2.jpg" alt=""></a>
                </div>
                <a href="#">Офлайн-редактор PrintBook.ru</a>
                <p>Удобная и легкая в использованиии программа</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img3_3.jpg" alt=""></a>
                </div>
                <a href="#">SMS-информирование</a>
                <p>Узнавайте о статусах вашего заказа, его готовности раньше всех</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img3_1.jpg" alt=""></a>
                </div>
                <a href="#">Мобильные приложения</a>
                <p>Новинка: netPrint.ru в вашем iPhone или Android!</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img3_2.jpg" alt=""></a>
                </div>
                <a href="#">Офлайн-редактор PrintBook.ru</a>
                <p>Удобная и легкая в использованиии программа</p>
            </li>
            <li>
                <div class="slider__img">
					<a href=""><img src="images/slider/img3_3.jpg" alt=""></a>
                </div>
                <a href="#">SMS-информирование</a>
                <p>Узнавайте о статусах вашего заказа, его готовности раньше всех</p>
            </li>
        </ul>
    </div>
</div>