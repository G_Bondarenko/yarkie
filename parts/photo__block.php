<div class="article__block article__block-photo">
  <img src="images/bg/img1.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Фотографии</h2>
                <p class="mobile__hide">
                    Классика жанра – ваши фотографии 10х15, 10х13,5, 15х21 и вариации.
                    На ваш выбор фотобумага от признанных лидеров в производстве
                    фотоматериалов. Самый популярный формат Стандарт; Премиум – для
                    тех, кто выбирает качество на долгие годы; Металлик подчеркивает
                    особенные фото серебристым свечением. 
                </p>
                <p class="mobile__display">
                	«Яркие воспоминания на годы»
                </p>
                <div>
                    <a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
                </div>
            </div>
        </div>
    </div>
</div>