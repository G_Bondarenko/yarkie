<div class="article__block article__block-photo block__gal">
  <img src="images/bg/img9.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Фотографии</h2>
                <p>
                    Классика жанра – ваши фотографии 10х15, 10х13,5, 15х21 и вариации.
					Самый популярный формат Стандарт; Премиум – для тех, кто выбирает
					качество на долгие годы; Металлик подчеркивает особенные фото
					серебристым свечением. Матовая или глянец – какой вы предпочтёте?
                </p>
            </div>
        </div>
    </div>
</div>