<div class="footer">
    <div class="wrapper">
        <div class="clearfix">
            <div class="footer__menu">
                <div class="footer__menu-col">
                    <h3>Разделы</h3>
                    <ul>
                        <li>
                            <a href="#">Фотографии</a>
                        </li>
                        <li>
                            <a href="#">Фотокалендари</a>
                        </li>
                        <li>
                            <a href="#">Фотосувениры</a>
                        </li>
                        <li>
                            <a href="#">Интерьерная печать</a>
                        </li>
                    </ul>
                    <h3>Доставка и оплата</h3>
                    <ul>
                        <li>
                            <a href="#">Доставка заказов</a>
                        </li>
                        <li>
                            <a href="#">Москва</a>
                        </li>
                        <li>
                            <a href="#">Все способы доставки</a>
                        </li>
                        <li>
                            <a href="#">Оплата заказов</a>
                        </li>
                        <li>
                            <a href="#">Оплата для юр.лиц</a>
                        </li>
                        <li>
                            <a href="#">Все способы оплаты</a>
                        </li>
                    </ul>
                </div>
                <div class="footer__menu-col">
                    <h3>Услуги и сервисы</h3>
                    <ul>
                        <li>
                            <a href="#">Клиентам</a>
                        </li>
                        <li>
                            <a href="#">Личный фотоархив</a>
                        </li>
                        <li>
                            <a href="#">Мобильные приложения</a>
                        </li>
                        <li>
                            <a href="#">Загрузка фотографий</a>
                        </li>
                        <li>
                            <a href="#">Альбомы для друзей</a>
                        </li>
                        <li>
                            <a href="#">Фотографии из соцсетей</a>
                        </li>
                        <li>
                            <a href="#">SMS-информирование</a>
                        </li>
                        <li>
                            <a href="#">Сканирование пленок</a>
                        </li>
                        <li>
                            <a href="#">Офлайн-редактор PRINTBOOK</a>
                        </li>
                        <li>
                            <a href="#">Онлайн-редактор</a>
                        </li>
                        <li>
                            <a href="#">Дизайнеры в помощь</a>
                        </li>
                        <li>
                            <a href="#">Мои сайты</a>
                        </li>
                        <li>
                            <a href="#">Программа «Пригласи друга»</a>
                        </li>
                    </ul>
                </div>
                <div class="footer__menu-col">
                    <h3>Помощь</h3>
                    <ul>
                        <li>
                            <a href="#">Справочная служба</a>
                        </li>
                        <li>
                            <a href="#">Где мой заказ?</a>
                        </li>
                        <li>
                            <a href="#">Проверить сертификат</a>
                        </li>
                        <li>
                            <a href="#">Вопрос - ответ</a>
                        </li>
                        <li>
                            <a href="#">Задать вопрос</a>
                        </li>
                        <li>
                            <a href="#">Поиск по сайту</a>
                        </li>
                        <li>
                            <a href="#">Помощь</a>
                        </li>
                        <li>
                            <a href="#">Регистрация</a>
                        </li>
                        <li>
                            <a href="#">Печать</a>
                        </li>
                        <li>
                            <a href="#">Работа с системой</a>
                        </li>
                        <li>
                            <a href="#">Доставка и оплата</a>
                        </li>
                        <li>
                            <a href="#">Акции и конкурсы</a>
                        </li>
                        <li>
                            <a href="#">PRINTBOOK.ru</a>
                        </li>
                        <li>
                            <a href="#">Отписаться от рассылки</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer__widget">
                <!-- VK Widget -->
                <div id="vk_groups"></div>                
                <ul class="widget__a">
                    <li>
                        <a href="#"><img src="images/icons/icon11.jpg" alt=""></a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icons/icon12.jpg" alt=""></a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icons/icon13.jpg" alt=""></a>
                    </li>
                    <li>
                        <a href="#"><img src="images/icons/icon14.jpg" alt=""></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="footer__contact">
            <div class="footer__menu">
                <div class="clearfix">
                    <div class="footer__menu-col">
                        <span class="copy">© 2004-2015 netPrint.ru</span>
                    </div>
                    <div class="footer__menu-col">
                        <span class="footer__phone">+7(495) 601-99-99</span>
                    </div>
                </div>
                <p>
                    Национальный сервис цифровой фотопечати №1 в России по количеству пользователей
                </p>
            </div>
            <div class="footer__widget">
                <p>
                    <a href="">Соглашение по использованию сервиса </a>
                    <img src="images/template/logo__footer.png" alt="">
                </p>
            </div>
        </div>
    </div>
</div>
<div class="footer__bottom">
    <div class="wrapper">
        <div class="clearfix">
            <ul class="paypal">
                <li>
                   <a href="#">
                       <img src="images/icons/icon7.png" alt="">
                   </a> 
                </li>
                <li>
                   <a href="#">
                       <img src="images/icons/icon8.png" alt="">
                   </a> 
                </li>
                <li>
                   <a href="#">
                       <img src="images/icons/icon9.png" alt="">
                   </a> 
                </li>
                <li>
                   <a href="#">
                       <img src="images/icons/icon10.png" alt="">
                   </a> 
                </li>
            </ul>
            <a href="#" class="left a__app">
            	<img src="images/template/app.png" alt="" class="mobile__hide">
            	<img src="images/template/app_mobile.png" alt="" class="mobile__display">
            </a>
            <a href="#" class="left a__google">
            	<img src="images/template/google.png" alt=""  class="mobile__hide">
            	<img src="images/template/google_mobile.png" alt=""  class="mobile__display">
            </a>
            <div class="socials right">
                <p>
                    Мы в соц.сетях:
                </p>
                <ul>
                    <li>
                        <a href="#" class="vk"></a>
                    </li>
                    <li>
                        <a href="#" class="fb"></a>
                    </li>
                    <li>
                        <a href="#" class="ok"></a>
                    </li>
                    <li>
                        <a href="#" class="in"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<a href="javascript:void(0)" id="up" class="up"></a>