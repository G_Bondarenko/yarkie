<div class="calendar__item action__ul">
   <div class="wrapper">
        <h2>Акции и бонусы</h2>
	</div>
    <div class="calendar__item-ul">
        <div>
            <ul class="wrapper">
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_1.jpg" alt=""></a>
						<div class="action__day">
							Остался 1 день
						</div>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Заголовок для блока</a>
					<p>Краткое описание для этой акции в две строки</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_2.jpg" alt=""></a>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Еще одни заголовок для блока</a>
					<p>Другое описание акции в две строки для этого блока</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_3.jpg" alt=""></a>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Другой заголовок</a>
					<p>Краткое описание для этой акции в две строки</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
			</ul>
        </div>
        <div class="alt">
        	<ul class="wrapper">
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_4.jpg" alt=""></a>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Заголовок для блока</a>
					<p>Краткое описание для этой акции в две строки</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_5.jpg" alt=""></a>
						<div class="action__day">
							Остался 1 день
						</div>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Еще одни заголовок для блока</a>
					<p>Другое описание акции в две строки для этого блока</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_6.jpg" alt=""></a>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Другой заголовок</a>
					<p>Краткое описание для этой акции в две строки</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
			</ul>
        </div>
        <div>
        	<ul class="wrapper">
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_7.jpg" alt=""></a>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Заголовок для блока</a>
					<p>Краткое описание для этой акции в две строки</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_8.jpg" alt=""></a>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Еще одни заголовок для блока</a>
					<p>Другое описание акции в две строки для этого блока</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
				<li>
					<div class="slider__img">
						<a href=""><img src="images/slider/img2_9.jpg" alt=""></a>
					</div>
					<p class="p__date">11.09.2015 - 25.10.2015</p>
					<a href="#">Другой заголовок</a>
					<p>Краткое описание для этой акции в две строки</p>
					<a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
				</li>
			</ul>
        </div>
    </div>
</div>