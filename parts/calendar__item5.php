<div class="calendar__item">
    <div class="calendar__item-ul">
        <div>
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_1.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук ROYAL в твердой персональной фотообложке</a>
                    <p>
                        Большие фотографии не только украсят
						ваш интерьер, но и станут классным
						подарком друзьям и близким.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_2.jpg" alt=""></a>
                    </div>
                    <a href="#">Планшеты ROYAL</a>
                    <p>
                        В Планшете ROYAL прекрасно впишутся
						история одного особо памятного дня
						или сказка для вашего малыша.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_3.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук ROYAL в кожаной обложке</a>
                    <p>
                        Фотобумага Шёлк или Металлик и элементы
						декорирования обложки непременно
						порадуют вас.
                    </p>
                    <div class="slider__block-price">
                        от
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        <div class="alt">
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_4.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук в твердой персональной фотообложке</a>
                    <p>
                        Отличный вариант подарка, который
						может быть приурочен к любому событию.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_5.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук в мягкой персональной фотообложке</a>
                    <p>
                        Выглядит более молодежно
						и непринужденно. В такой книге будут
						отлично смотреться кадры со дня рождения,
						вечеринки или прогулок.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_6.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук Премиум в твердой персональной фотообложке</a>
                    <p>
                        Подойдет для оформления памятных
						событий и праздников.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        
        <div>
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_7.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук Премиум в мягкой персональной фотообложке</a>
                    <p>
                        Плотные страницы из фотобумаги надолго
						сохранят ваши кадры, а благодаря обложке
						она выглядит более молодежно
						и непринужденно.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_8.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук Премиум в твердой обложке из искусственной кожи</a>
                    <p>
                        Эффектный внешний вид фотокниги
						придает ей статусность и позволяет
						преподнести книгу в подарок не только
						друзьям и близким, но и деловым
						партнерам  клиентам и коллегам.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_9.jpg" alt=""></a>
                    </div>
                    <a href="#">Минибуки</a>
                    <p>
                        Вариант фотокниги, исполненный
						в миниатюре. Плотные страницы из
						фотобумаги надолго сохранят ваши кадры.
						Удобно хранить, носить с собой
						и демонстрировать.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        <div class="alt">
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_10.jpg" alt=""></a>
                    </div>
                    <a href="#">Планшеты Премиум</a>
                    <p>
                        Создан для подборки ваших любимых
						фотографий. Цельные развороты страниц
						позволяют размещать фотографию сразу
						на двух листах. Подходит для выпускных
						альбомов.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_11.jpg" alt=""></a>
                    </div>
                    <a href="#">Принтбук в твердой обложке из искусственной кожи</a>
                    <p>
                        Достаточно строгая и в тоже время
						изящная обложка из кожзаменителя
						идеально подойдет для оформления книги
						для друзей, родственников или коллег.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img15_12.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотоброшюры</a>
                    <p>
                        Удобный вариант для создания рекламных
						буклетов, каталогов, фотоотчетов
						о мероприятиях, мини-портфолио и тому
						подобных книжиц индивидуальным
						тиражом.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
    </div>    
</div>