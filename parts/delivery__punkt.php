
<div class="delivery__punkt">
	<div class="wrapper">
		<h2>
			Пункты выдачи
			<a href="#" class="mobile__hide">Посмотреть сразу все пункты на карте</a>
			<a href="#" class="mobile__display">Все пункты на карте</a>
		</h2>
		<ul class="delivery__punkt-ul">
			<li>
				<div class="logo">
					<img src="images/content/img2_1.jpg" alt="" class="mobile__hide">
					<img src="images/content/img2_1_mobile.jpg" alt="" class="mobile__display">
				</div>
				<p>
					Более 250 точек выдачи заказов
					в Москве и по всей России.
					Располагаются обычно в удобной
					транспортной и пешей доступности.
				</p>
				<h4>Способы оплаты:</h4>
				<h6>Предоплата или постоплата</h6>
				<ul>
					<li>
						Наличные
					</li>
					<li>
						Банковские карты
					</li>
					<li>
						Online-платежи
					</li>
				</ul>
				<a href="#">Посмотреть пункты на карте</a>
				<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
			</li>
			<li>
				<div class="logo">
					<img src="images/content/img2_2.jpg" alt="">
				</div>
				<p>
					Более 250 точек выдачи заказов
					в Москве и по всей России.
					Располагаются обычно в удобной
					транспортной и пешей доступности.
				</p>
				<h4>Способы оплаты:</h4>
				<h6>Предоплата или постоплата</h6>
				<ul>
					<li>
						Наличные
					</li>
					<li>
						Банковские карты
					</li>
					<li>
						Online-платежи
					</li>
				</ul>
				<a href="#">Посмотреть пункты на карте</a>
				<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
			</li>
			<li>
				<div class="logo">
					<img src="images/content/img2_3.jpg" alt="">
				</div>
				<p>
					Более 250 точек выдачи заказов
					в Москве и по всей России.
					Располагаются обычно в удобной
					транспортной и пешей доступности.
				</p>
				<h4>Способы оплаты:</h4>
				<h6>Предоплата или постоплата</h6>
				<ul>
					<li>
						Наличные
					</li>
					<li>
						Банковские карты
					</li>
					<li>
						Online-платежи
					</li>
				</ul>
				<a href="#">Посмотреть пункты на карте</a>
				<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
			</li>
			<li>
				<div class="logo">
					<img src="images/content/img2_4.jpg" alt="">
				</div>
				<p>
					Более 250 точек выдачи заказов
					в Москве и по всей России.
					Располагаются обычно в удобной
					транспортной и пешей доступности.
				</p>
				<h4>Способы оплаты:</h4>
				<h6>Предоплата или постоплата</h6>
				<ul>
					<li>
						Наличные
					</li>
					<li>
						Банковские карты
					</li>
					<li>
						Online-платежи
					</li>
				</ul>
				<a href="#">Посмотреть пункты на карте</a>
				<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
			</li>
			<li>
				<div class="logo">
					<img src="images/content/img2_5.jpg" alt="">
				</div>
				<p>
					Более 250 точек выдачи заказов
					в Москве и по всей России.
					Располагаются обычно в удобной
					транспортной и пешей доступности.
				</p>
				<h4>Способы оплаты:</h4>
				<h6>Предоплата или постоплата</h6>
				<ul>
					<li>
						Наличные
					</li>
					<li>
						Банковские карты
					</li>
					<li>
						Online-платежи
					</li>
				</ul>
				<a href="#">Посмотреть пункты на карте</a>
				<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</div>