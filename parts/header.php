<div class="header">
    <div id="top-cities-frame" class="header__inner">
        <div class="top-cities">
            <div id="cities-list" style="display: none;">
                <div class="clearfix wrapper">
                    <ul class="countries">
                        <li><a href="" onclick="closeTopMenu()">Вся Россия</a></li>
                        <li><a href="#" id="city_link_185" onclick="closeTopMenu()">Страны СНГ</a></li>
                    </ul>
                    <div class="clearfix">
                        <a href="javascript:void(0);" id="close_menu" class="close_menu" onclick="closeTopMenu()">Закрыть</a>
                        <input type="text" id="search_cities" placeholder="Укажите город" class="placeholder">
                        <input id="clear_city" type="image" src="images/close_menu.png">
                        <ul class="big_cities">
                            <li><a href="#" id="city_link_1">Москва и область</a></li>
                            <li><a href="#" id="city_link_3">Санкт-Петербург и область</a></li>
                        </ul>
                    </div>
                </div>
                <div class="letters__bg">
                    <div class="wrapper">
                        <ul class="letters">
                            <li><a href="#">А</a></li>
                            <li><a href="#">Б</a></li>
                            <li><a href="#">В</a></li>
                            <li><a href="#">Г</a></li>
                            <li><a href="#">Д</a></li>
                            <li><a href="#">Е</a></li>
                            <li><a href="#">Ж</a></li>
                            <li><a href="#">З</a></li>
                            <li><a href="#">И</a></li>
                            <li><a href="#">Й</a></li>
                            <li><a href="#">К</a></li>
                            <li><a href="#">Л</a></li>
                            <li><a href="#">М</a></li>
                            <li><a href="#">Н</a></li>
                            <li><a href="#">О</a></li>
                            <li><a href="#">П</a></li>
                            <li><a href="#">Р</a></li>
                            <li><a href="#">С</a></li>
                            <li><a href="#">Т</a></li>
                            <li><a href="#">У</a></li>
                            <li><a href="#">Ф</a></li>
                            <li><a href="#">Х</a></li>
                            <li><a href="#">Ч</a></li>
                            <li><a href="#">Ш</a></li>
                            <li><a href="#">Щ</a></li>
                            <li><a href="#">Э</a></li>
                            <li><a href="#">Ю</a></li>
                            <li><a href="#">Я</a></li>
                        </ul>
                    </div>
                </div>
                <div class="wrapper">
                    <h3 class="letter" style="display: none;">А</h3>
                    <div id="no_city_match_error" style="display: none;">По Вашему запросу городов не найдено</div>
                    <ul class="cities">
                        <li></li>
                    </ul>
                </div>
            </div>
            <div class="top-block">
                <div class="wrapper">
                    <div class="top_circles">
                       	<a href="javascript:void(0);" class="menu__ico"></a>
                        <a href="index.php" class="header__logo"></a>
                        <div id="city-active" class="header__city">
                            <a href="#" onclick="showCitiesList(this);return false;">Москва и область</a>
                        </div>
                        <div class="header__phone">8 495 601-96-96</div>
                        <a href="#" class="header__delivery">Доставка и оплата</a>
                        <a href="#" class="header__cart">Корзина <span>(0)</span></a>
                        <a href="#fancybox__auth" class="header__enter fancybox__auth">Войти</a>
                        <a href="#" class="header__help">Помощь</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="wrapper">
           	<div class="mobile__reg mobile__display">
                <a href="#fancybox__auth" class="fancybox__auth">Вход на сайт</a>
                <a href="#fancybox__reg" class="fancybox__reg">Регистрация</a>
           	</div>
            <ul>
                <li>
                    <a href="javascript:void(0);">Фотографии</a>
                    <div class="drop__menu">
                        <div class="menu__col">
                            <h2>Принтбук Премиум</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Минибуки</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук Премиум</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Фотоброшюры</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук ROYAL</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Твердая кожаная обложка</a></li>
                                <li><a href="#">Планшеты ROYAL</a></li>
                                <li><a href="#">Фотоборшюры</a></li>
                                <li><a href="#">Все фотокниги Принтбук ROYAL</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Другие</h2>
                            <ul>
                                <li><a href="#">Сказки</a></li>
                                <li><a href="#">Тестовая фотоброшюра</a></li>
                                <li><a href="#">Фотокнига Instagram</a></li>
                                <li><a href="#">Фотокниги на выпускной</a></li>
                                <li><a href="#">Свадебные фотографии</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="active">
                    <a href="javascript:void(0);">Фотокниги</a>
                    <div class="drop__menu">
                        <div class="menu__col">
                            <h2>Принтбук Премиум</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Минибуки</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук Премиум</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Фотоброшюры</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук ROYAL</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Твердая кожаная обложка</a></li>
                                <li><a href="#">Планшеты ROYAL</a></li>
                                <li><a href="#">Фотоборшюры</a></li>
                                <li><a href="#">Все фотокниги Принтбук ROYAL</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Другие</h2>
                            <ul>
                                <li><a href="#">Сказки</a></li>
                                <li><a href="#">Тестовая фотоброшюра</a></li>
                                <li><a href="#">Фотокнига Instagram</a></li>
                                <li><a href="#">Фотокниги на выпускной</a></li>
                                <li><a href="#">Свадебные фотографии</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="javascript:void(0);">Фотокалендари</a>
                    <div class="drop__menu">
                        <div class="menu__col">
                            <h2>Принтбук Премиум</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Минибуки</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук Премиум</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Фотоброшюры</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук ROYAL</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Твердая кожаная обложка</a></li>
                                <li><a href="#">Планшеты ROYAL</a></li>
                                <li><a href="#">Фотоборшюры</a></li>
                                <li><a href="#">Все фотокниги Принтбук ROYAL</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="javascript:void(0);">Фотосувениры</a>
                    <div class="drop__menu">
                        <div class="menu__col">
                            <h2>Принтбук Премиум</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Минибуки</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук Премиум</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Фотоброшюры</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="javascript:void(0);">Интерьерная печать</a>
                    <div class="drop__menu">
                        <div class="menu__col">
                            <h2>Принтбук Премиум</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Минибуки</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук Премиум</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="javascript:void(0);">Акции и бонусы</a>
                    <div class="drop__menu">
                        <div class="menu__col">
                            <h2>Принтбук Премиум</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Минибуки</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук Премиум</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Мягкая персональная фотообложка</a></li>
                                <li><a href="#">Твердая обложка из искусственной кожи</a></li>
                                <li><a href="#">Фотоброшюры</a></li>
                                <li><a href="#">Планшеты Премиум</a></li>
                                <li><a href="#">Все фотокниги Принтбук</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Принтбук ROYAL</h2>
                            <ul>
                                <li><a href="#">Твердая персональная фотообложка</a></li>
                                <li><a href="#">Твердая кожаная обложка</a></li>
                                <li><a href="#">Планшеты ROYAL</a></li>
                                <li><a href="#">Фотоборшюры</a></li>
                                <li><a href="#">Все фотокниги Принтбук ROYAL</a></li>
                            </ul>
                        </div>
                        <div class="menu__col">
                            <h2>Другие</h2>
                            <ul>
                                <li><a href="#">Сказки</a></li>
                                <li><a href="#">Тестовая фотоброшюра</a></li>
                                <li><a href="#">Фотокнига Instagram</a></li>
                                <li><a href="#">Фотокниги на выпускной</a></li>
                                <li><a href="#">Свадебные фотографии</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#">Услуги и сервисы</a>
                </li>
                <li class="mobile__display">
                    <a href="#">Оплата и доставка</a>
				</li>
           		<li class="mobile__display">
                    <a href="#">Контакты</a>
				</li>
            </ul>
        </div>
    </div>
</div>