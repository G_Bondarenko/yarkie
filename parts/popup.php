<div id="popup" class="popup">
    <div class="text__center">
        <h1>Книга и стиль</h1>
        <ul class="popup__slider">
            <li>
                <div class="wrap"><img src="images/slider/img10_1.jpg" alt=""></div>
            </li>
            <li>
                <div class="wrap"><img src="images/slider/img10_1.jpg" alt=""></div>
            </li>
            <li>
                <div class="wrap"><img src="images/slider/img10_1.jpg" alt=""></div>
            </li>
        </ul>
        <h2 class="mobile__hide">Выбрать редактор</h2>
        <h2 class="mobile__display">Стиль «Ромашки»</h2>
    </div>
    <h2 class="mobile__display bg__grey">
    	Выбрать редактор
    </h2>
    <ul class="edit__list">
        <li>
            <div class="edit__list-img">
                <img src="images/content/img1_1.jpg" alt="" class="mobile__hide">
                <img src="images/content/img1_1_mobile.jpg" alt="" class="mobile__display">
                <img src="images/icons/icon11.png" class="edit__list-icon" alt="" class="mobile__hide">
                <img src="images/icons/icon11_mobile.png" class="edit__list-icon" alt="" class="mobile__display">
            </div>
            <h3>Онлайн-редактор</h3>
            <p>
                Краткое описание редактора
                Плавный переход интенсивных
                цветовых оттенков фотографии
                на прозрачном глянцевом пластике.
            </p>
            <div>
                <a href="#">Подробнее</a>
            </div>
            <a href="#" class="a__create">СОЗДАТЬ</a>
        </li>
        <li>
            <div class="edit__list-img">
                <img src="images/content/img1_2.jpg" alt="" class="mobile__hide">
                <img src="images/content/img1_2_mobile.jpg" alt="" class="mobile__display">
                <img src="images/icons/icon12.png" class="edit__list-icon" alt="" class="mobile__hide">
                <img src="images/icons/icon12_mobile.png" class="edit__list-icon" alt="" class="mobile__display">
            </div>
            <h3>Офлайн-редактор</h3>
            <p>
                Краткое описание редактора
                Плавный переход интенсивных
                цветовых оттенков фотографии
                на прозрачном глянцевом пластике.
            </p>
            <div>
                <a href="#">Подробнее</a>
            </div>
            <a href="#" class="a__create">СОЗДАТЬ</a>
        </li>
        <li>
            <div class="edit__list-img">
                <img src="images/content/img1_3.jpg" alt="" class="mobile__hide">
                <img src="images/content/img1_3_mobile.jpg" alt="" class="mobile__display">
                <img src="images/icons/icon13.png" class="edit__list-icon" alt="" class="mobile__hide">
                <img src="images/icons/icon13_mobile.png" class="edit__list-icon" alt="" class="mobile__display">
            </div>
            <h3>Экспресс-редактор</h3>
            <p>
                Краткое описание редактора
                Плавный переход интенсивных
                цветовых оттенков фотографии
                на прозрачном глянцевом пластике.
            </p>
            <div>
                <a href="#">Подробнее</a>
            </div>
            <a href="#" class="a__create">СОЗДАТЬ</a>
        </li>
    </ul>
</div>