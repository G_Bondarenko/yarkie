<div class="calendar__item">
    <div class="calendar__item-ul">
        <div>
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img13_1.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотографии</a>
                    <p>
                        Большие фотографии не только украсят
						ваш интерьер, но и станут классным
						подарком друзьям и близким.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img13_2.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотографии большого формата</a>
                    <p>
                        Этот нестандартный формат печати
						идеально подойдет для изображений
						с пропорциями сторон 1:3 или созданных
						вручную панорамных снимков. 
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img13_3.jpg" alt=""></a>
                    </div>
                    <a href="#">Панорманые фото</a>
                    <p>
                        Классика жанра – ваши фотографии 10х15,
						10х13,5, 15х21 и вариации. На ваш выбор
						фотобумага от признанных лидеров
						в производстве фотоматериалов
                    </p>
                    <div class="slider__block-price">
                        от
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        <div class="alt">
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img13_4.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотографии с подписью</a>
                    <p>
                        Большие фотографии не только украсят
						ваш интерьер, но и станут классным
						подарком друзьям и близким.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img13_5.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотографии с Polaroid</a>
                    <p>
                        Этот нестандартный формат печати
						идеально подойдет для изображений
						с пропорциями сторон 1:3 или созданных
						вручную панорамных снимков.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img13_6.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотографии 10х10 с Instagram</a>
                    <p>
                        Подойдет для оформления памятных
						событий и праздников.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        
        <div>
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img13_7.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотографии с оформлением</a>
                    <p>
                        Классика жанра – ваши фотографии 10х15,
						10х13,5, 15х21 и вариации. На ваш выбор
						фотобумага от признанных лидеров
						в производстве фотоматериалов.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
    </div>    
</div>