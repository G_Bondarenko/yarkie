<div class="format__block">
   <div class="wrapper">
        <h2>Выберите формат:</h2>
        <ul class="format__block-ul">
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="0"><img src="images/slider/img8_1.jpg" alt=""></a>
                </div>
                <p>Название товара в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="0" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="1"><img src="images/slider/img8_2.jpg" alt=""></a>
                </div>
                <p>Еще одно название в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="1" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="2"><img src="images/slider/img8_3.jpg" alt=""></a>
                </div>
                <p>Название товара в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="2" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="3"><img src="images/slider/img8_4.jpg" alt=""></a>
                </div>
                <p>Еще одно название в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="3" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="4"><img src="images/slider/img8_1.jpg" alt=""></a>
                </div>
                <p>Название товара в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="4" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="0"><img src="images/slider/img8_1.jpg" alt=""></a>
                </div>
                <p>Название товара в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="0" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="1"><img src="images/slider/img8_2.jpg" alt=""></a>
                </div>
                <p>Еще одно название в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="1" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="2"><img src="images/slider/img8_3.jpg" alt=""></a>
                </div>
                <p>Название товара в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="2" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="3"><img src="images/slider/img8_4.jpg" alt=""></a>
                </div>
                <p>Еще одно название в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="3" class="a__choose">Выбрать</a>
            </li>
            <li>
                <div class="slider__img">
                    <a href="javascript:void(0);" data-index="4"><img src="images/slider/img8_1.jpg" alt=""></a>
                </div>
                <p>Название товара в две строки</p>
                <div class="slider__block-price">
                    <span class="new__price">590</span>
                </div>
                <a href="javascript:void(0);" data-index="4" class="a__choose">Выбрать</a>
            </li>
        </ul>
    </div>
</div>