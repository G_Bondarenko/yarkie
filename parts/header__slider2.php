<ul class="header__slider clearfix">
    <li>
        <div class="wrapper">
           <div class="vertical__align">
                <img src="images/slider/img1.jpg" class="slider__bg" alt="">
                <h1>Заголовок промо-акции в две строки</h1>
                <p>Краткий текст с описанием акции: для кого она, какие преимущества и скидки дает посетителям этого сайта</p>
                <img src="images/template/slider__icon.png" class="slider__icon" alt="">
            </div>
        </div>
    </li>
</ul>