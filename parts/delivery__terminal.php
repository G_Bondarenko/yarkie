<div class="delivery__terminal">
	<div class="wrapper">
		<ul class="delivery__terminal-ul">
			<li>
				<h2>Териминалы обслуживания</h2>
				<div class="delivery__terminal-li">
					<div class="logo">
						<img src="images/content/img2_6.jpg" alt="" class="mobile__hide">
						<img src="images/content/img2_7.jpg" alt="" class="mobile__hide">
						<img src="images/content/img2_6_mobile.jpg" alt="" class="mobile__display">
						<img src="images/content/img2_7_mobile.jpg" alt="" class="mobile__display">
					</div>
					<p>
						Заказы хранятся в металлических ячейках
						автоматизированных терминалов – постоматах. 
					</p>
					<a href="#">Подробнее о доставке</a>
					<h4>Способы оплаты:</h4>					
					<div class="block__delivery__info clearfix">
						<div class="left">
							<h6>Предоплата или постоплата</h6>
							<ul>
								<li>
									Наличные
								</li>
								<li>
									Банковские карты
								</li>
								<li>
									Online-платежи
								</li>
							</ul>
							<a href="#">Подробнее о доставке</a>
						</div>						
						<div class="delivery__info mobile__hide">
							Действуют ограничения<br>
							по габаритам: PickPoint –<br>
							от 36х36х60 и выше, <br>
							QIWI Post – от 50х75.
						</div>
						<div class="delivery__info mobile__display">
							Действуют ограничения по габаритам:
							PickPoint – от 36х36х60 и выше, <br>
							QIWI Post – от 50х75.
						</div>
					</div>
					<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
				</div>
			</li>
			<li>
				<h2>Курьер</h2>
				<div class="delivery__terminal-li">
					<div class="logo">
						<img src="images/content/img2_8.jpg" alt="" class="mobile__hide">
						<img src="images/content/img2_8_mobile.jpg" alt="" class="mobile__display">
					</div>
					<p>
						Курьер привезет готовый заказ по удобному вам
						адресу: на дом или в офис.
					</p>
					<a href="#">Подробнее о доставке</a>
					<h4>Способы оплаты:</h4>					
					<div class="clearfix block__delivery__info">
						<div class="left">
							<h6>Предоплата или постоплата</h6>
							<ul>
								<li>
									Наличные
								</li>
								<li>
									Банковские карты
								</li>
								<li>
									Online-платежи
								</li>
							</ul>
							<a href="#">Подробнее о доставке</a>
						</div>						
						<div class="delivery__info">
							Доставка по рабочим<br>
							дням с 9 до 19 часов.
						</div>
					</div>
					<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
				</div>
			</li>
		</ul>
	</div>
</div>
<div class="delivery__terminal grey">
	<div class="wrapper">
		<ul class="delivery__terminal-ul">
			<li>
				<h2>Почта России</h2>
				<div class="delivery__terminal-li">
					<div class="logo">
						<img src="images/content/img2_9.jpg" alt="">
					</div>
					<p>
						Заказы хранятся в металлических ячейках
						автоматизированных терминалов – постоматах. 
					</p>
					<a href="#">Подробнее о доставке</a>
					<h4>Способы оплаты:</h4>					
					<div class="block__delivery__info clearfix">
						<div class="left">
							<h6>Предоплата или постоплата</h6>
							<ul>
								<li>
									Наличные
								</li>
								<li>
									Банковские карты
								</li>
								<li>
									Online-платежи
								</li>
							</ul>
							<a href="#">Подробнее о доставке</a>
						</div>						
						<div class="delivery__info">
							При оплате наложенным<br>
							платежом взимается<br>
							комиссия.
						</div>
					</div>
					<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
				</div>
			</li>
			<li>
				<h2>Курьерские службы</h2>
				<div class="delivery__terminal-li">
					<div class="logo">
						<img src="images/content/img2_10.jpg" alt="">
						<img src="images/content/img2_11.jpg" alt="">
					</div>
					<p>
						Курьер привезет готовый заказ по удобному вам
						адресу: на дом или в офис.
					</p>
					<a href="#">Подробнее о доставке</a>
					<h4>Способы оплаты:</h4>					
					<div class="clearfix block__delivery__info">
						<div class="left">
							<h6>Предоплата или постоплата</h6>
							<ul>
								<li>
									Наличные
								</li>
								<li>
									Банковские карты
								</li>
								<li>
									Online-платежи
								</li>
							</ul>
							<a href="#">Подробнее о доставке</a>
						</div>	
					</div>
					<div class="li__detail">
					<ul>
						<li class="li1">
							<div class="img">
								<img src="images/content/img3_1.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_1_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Стоимость</p>
							<p class="p__price">от 150 руб.</p>
						</li>
						<li class="li2">
							<div class="img">
								<img src="images/content/img3_2.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_2_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок доставки</p>
							<p class="p__price">от 1 дн</p>
						</li>
						<li class="li3">
							<div class="img">
								<img src="images/content/img3_3.jpg" alt="" class="mobile__hide">
								<img src="images/content/img3_3_mobile.jpg" alt="" class="mobile__display">
							</div>
							<p>Срок хранения</p>
							<p class="p__price">14 дн</p>
						</li>
					</ul>
				</div>
				</div>
			</li>
		</ul>
	</div>
</div>