<div class="article__block article__block-photo">
  <img src="images/bg/img3.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Фотокниги</h2>
                <p class="mobile__hide">
                    Фотокнига для самых значимых фотографий, которые стоит передать
                    по наследству внукам. Фотопечать не померкнет и через 100 лет,
                    а особо плотные страницы и обложка надежно защитят ваши
                    фотографии от повреждений.
                </p>
                <p class="mobile__display">
                	«Ваша солнечная история»
                </p>
                <div>
                    <a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
                </div>
            </div>
        </div>
    </div>
</div>