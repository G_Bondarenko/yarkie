<div class="calendar__item">
    <div class="calendar__item-ul">
        <div>
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
                        <a href=""><img src="images/slider/img12_1.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотохолст</a>
                    <p>
                        Изготовленное на основе хлопковой ткани,
						на которую наносится изображение.
						Печатайте на холсте ваши фотографию,
						рисунок или репродукцию – они будут
						выглядеть как настоящая картина.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_2.jpg" alt=""></a>
                    </div>
                    <a href="#">Пенокартон</a>
                    <p>
                        Прочный и невероятно легкий материал,
						насыщенность красок и четкие линии
						изображения при самом высоком
						разрешении печати. Даже самые большие
						форматы – ЛЕГКО!
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_3.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотобокс</a>
                    <p>
                        Единственные в России объемные
						фотокартины на основе пенокартона.
						Легкая конструкция напоминающая картину
						и портреты ваших близких.
                    </p>
                    <div class="slider__block-price">
                        от
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        <div class="alt">
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_4.jpg" alt=""></a>
                    </div>
                    <a href="#">Акрил</a>
                    <p>
                        Плавный переход интенсивных цветовых
						оттенков фотографии на прозрачном
						глянцевом пластике. Необычный материал
						создает ощущение объемности и глубокой
						насыщенности картины.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_5.jpg" alt=""></a>
                    </div>
                    <a href="#">Холст в багете</a>
                    <p>
                        Позволит превратить любую вашу
						фотографию в изысканный шедевр.
						Текстура холста под лён и фотопечать 
						отличный вариант для создания домашней
						картинной галереи.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_6.jpg" alt=""></a>
                    </div>
                    <a href="#">Пенокартон в багете</a>
                    <p>
                        Лёгкость пенокартона плюс невесомость
						алюминиевого багета. Отличное сочетание
						материалов подчеркнёт яркость
						и насыщенность фотопечати .
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        
        <div>
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_7.jpg" alt=""></a>
                    </div>
                    <a href="#">Модульная картина</a>
                    <p>
                        Выразительный элемент интерьера,
						способный в одиночку создать стиль всего
						помещения. Яркая, сочная и качественная
						печать  на пенокартоне.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_8.jpg" alt=""></a>
                    </div>
                    <a href="#">Фотобокс премиум</a>
                    <p>
                        Сочетает в себе лёгкость короба
						из пенокартона и текстуру льняного
						полотна. Ваши картины будут не только
						яркими и объёмными, но и необычными.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_9.jpg" alt=""></a>
                    </div>
                    <a href="#">Накатка на пенокартон</a>
                    <p>
                        Ваши картины с любимым качеством
						фотографий Премиум, глянцевыми бликами
						или матовой мягкостью, подчёркивающие
						тысячи оттенков и переходов цвета.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
        <div class="alt">
            <ul class="wrapper">
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_10.jpg" alt=""></a>
                    </div>
                    <a href="#">Пластификация</a>
                    <p>
                        Порадует надёжностью конструкции,
						лёгкостью восприятия и яркостью красок.
						А многообразие областей применения
						удивительно: гостиная, спальня, кухня,
						холл офиса
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_11.jpg" alt=""></a>
                    </div>
                    <a href="#">Печать по дереву</a>
                    <p>
                        Необычный и очень эффектный способ
						демонстрации ваших фоторабот. Снимки
						печатаются на холстах разного формата
						и поставляются вместе с крепежным
						набором.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
                <li>
                    <div class="calendar__item-img">
						<a href=""><img src="images/slider/img12_12.jpg" alt=""></a>
                    </div>
                    <a href="#">Картина с паспарту</a>
                    <p>
                        Предлагаем вам необычную идею
						памятного подарка – фотопазл. 5 форматов
						пазлов, из 24, 50, 204, 408 и 1000 элементов.
                    </p>
                    <div class="slider__block-price">
                        <dt>от</dt>
                        <span class="new__price">590</span>
                    </div>
                    <a href="#" class="a__butt">ВЫБРАТЬ</a>
                </li>
            </ul>
        </div>
    </div>    
</div>