<ul id="header__slider" class="header__slider">
    <li>
        <div class="wrapper">
           <div class="vertical__align">
                <a href="#"><img src="images/slider/img1.jpg" class="slider__bg" alt=""></a>
                <h1>Заголовок промо-акции в две строки</h1>
                <p>Краткий текст с описанием акции: для кого она, какие преимущества и скидки дает посетителям этого сайта</p>
                <img src="images/template/slider__icon.png" class="slider__icon" alt="">
                <a href="#" class="a__butt">ПОДРОБНЕЕ</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wrapper">
            <div class="vertical__align">
                <a href="#"><img src="images/slider/img1.jpg" class="slider__bg" alt=""></a>
                <h1>Заголовок промо-акции</h1>
                <p>Краткий текст с описанием акции: для кого она, какие преимущества и скидки дает посетителям этого сайта</p>
                <img src="images/template/slider__icon.png" class="slider__icon" alt="">
                <a href="#" class="a__butt">ПОДРОБНЕЕ</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wrapper">
            <div class="vertical__align">
                <a href="#"><img src="images/slider/img1.jpg" class="slider__bg" alt=""></a>
                <h1>Заголовок промо-акции в две строки</h1>
                <p>Краткий текст с описанием акции: для кого она, какие преимущества и скидки дает посетителям этого сайта</p>
                <img src="images/template/slider__icon.png" class="slider__icon" alt="">
                <a href="#" class="a__butt">ПОДРОБНЕЕ</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wrapper">
            <div class="vertical__align">
                <a href="#"><img src="images/slider/img1.jpg" class="slider__bg" alt=""></a>
                <h1>Заголовок промо-акции</h1>
                <p>Краткий текст с описанием акции: для кого она, какие преимущества и скидки дает посетителям этого сайта</p>
                <img src="images/template/slider__icon.png" class="slider__icon" alt="">
                <a href="#" class="a__butt">ПОДРОБНЕЕ</a>
            </div>
        </div>
    </li>
    <li>
        <div class="wrapper">
            <div class="vertical__align">
                <a href="#"><img src="images/slider/img1.jpg" class="slider__bg" alt=""></a>
                <h1>Заголовок промо-акции в две строки</h1>
                <p>Краткий текст с описанием акции: для кого она, какие преимущества и скидки дает посетителям этого сайта</p>
                <img src="images/template/slider__icon.png" class="slider__icon" alt="">
                <a href="#" class="a__butt">ПОДРОБНЕЕ</a>
            </div>
        </div>
    </li>
</ul>