<div class="tovar__info">
    <div class="wrapper clearfix">
        <div class="img__desc">
           <div class="tovar__slider__wrap">
                <ul class="tovar__slider">
                    <li><img src="images/slider/img4_1.jpg" /></li>
                    <li><img src="images/slider/img4_2_sm.jpg" /></li>
                    <li><img src="images/slider/img4_3_sm.jpg" /></li>
                    <li>
                       <iframe width="670" height="377" class="mobile__hide" src="https://www.youtube-nocookie.com/embed/jB1-48EjvrY?controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                       <iframe width="490" height="379" class="mobile__display" src="https://www.youtube-nocookie.com/embed/jB1-48EjvrY?controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    </li>
                </ul>

                <div id="tovar__slider-pager" class="tovar__slider-pager">
                    <a data-slide-index="0" href=""><img src="images/slider/img4_1_sm.jpg" /></a>
                    <a data-slide-index="1" href=""><img src="images/slider/img4_2_sm.jpg" /></a>
                    <a data-slide-index="2" href=""><img src="images/slider/img4_3_sm.jpg" /></a>
                    <a data-slide-index="3" href=""><img src="images/slider/img4_3_sm.jpg" /><img src="images/template/play.png" class="a__play" alt=""></a>
                </div>
            </div>
            <h5>
                Принтбук ROYAL – фотокнига премиального класса. Она выгодно
                подчёркивает вкус и стиль обладателя такой вещи.
            </h5>
            <p>
                Оцените приятную тяжесть книги, наполнив страницы своими
                фотографиями. Страницы, усиленные картоном, с различными 
                вариантами бумаги: фотобумага; фотобумага Металлик; фотобумага
                Шёлк – выбирайте подходящее решение для хранения самых значимых
                фотографий и событий. 
            </p>
        </div>
        <div class="tovar__detail">
            <h1>Название выбранного товара</h1>
            <div class="tovar__detail-price">
                <dt>от</dt> <span class="new__price">3 200 </span> <span class="old__price">3 600 P</span>
            </div>
            
            <h5>Выберите формат</h5>
            <select id="select__format" name="select__format" >
                <option value="0">20x20</option>
                <option value="1">30x30</option>
                <option value="2">40x40</option>
                <option value="3">50x50</option>
                <option value="4">60x60</option>
                <option value="5">60x60</option>
                <option value="6">70x70</option>
                <option value="7">80x80</option>
                <option value="8">90x90</option>
                <option value="9">100x100</option>
            </select>
            
            <h5>Выберите обложку</h5>
            <select name="select__format" >
                <option value="20">Коричневая кожа</option>
                <option value="30">Другое название</option>
                <option value="40">Еще одно название</option>
                <option value="50">Четвертое название</option>
                <option value="60">Другое название</option>
                <option value="70">Другое название</option>
                <option value="80">Другое название</option>
            </select>
            
            <h5>Выберите бумагу</h5>
            <select name="select__format" >
                <option value="20">Коричневая кожа</option>
                <option value="30">Другое название</option>
                <option value="40">Еще одно название</option>
                <option value="50">Четвертое название</option>
                <option value="60">Другое название</option>
                <option value="70">Другое название</option>
                <option value="80">Другое название</option>
            </select>
            
            <div><a href="#popup" class="fancybox a__butt a__submit">ОФОРМИТЬ ТОВАР</a></div>
            
            <ul class="specifications mobile__hide">                
                <li class="alt">
                    <span class="left">Тип обложки:</span>
                    <span class="right">твердая ламинированная</span>
                </li>
                <li>
                    <span class="left">Переплет: </span>
                    <span class="right">проклейка термолистом</span>
                </li>
                <li class="alt">
                    <span class="left">Страницы:</span>
                    <span class="right">фотобумага</span>
                </li>
                <li>
                    <span class="left">Печать:</span>
                    <span class="right">фотопечать</span>
                </li>
                <li class="alt">
                    <span class="left">Количество разворотов: </span>
                    <span class="right">от 10 до 30</span>
                </li>
                <li>
                    <span class="left">Разрешение печати: </span>
                    <span class="right">300 dpi</span>
                </li>
                <li class="alt">
                    <span class="left">Выполение заказа:</span>
                    <span class="right">2 дня</span>
                </li>
            </ul>
        </div>        
    </div>
    <div class="wrapper">
    	<div class="mobile__display drop__block">
        	<h2>
        		Описание
        	</h2>
			<div class="drop__block-inner">
				<h5>
					Принтбук ROYAL – фотокнига премиального класса. Она выгодно
					подчёркивает вкус и стиль обладателя такой вещи.
				</h5>
				<p>
					Оцените приятную тяжесть книги, наполнив страницы своими
					фотографиями. Страницы, усиленные картоном, с различными 
					вариантами бумаги: фотобумага; фотобумага Металлик; фотобумага
					Шёлк – выбирайте подходящее решение для хранения самых значимых
					фотографий и событий. 
				</p>
			</div>            
        </div>
        <div class="mobile__display drop__block specifications">
        	<h2>Характеристики</h2>
        	<div class="drop__block-inner">
				<ul>
					<li class="alt">
						<span class="left">Тип обложки:</span>
						<span class="right">твердая ламинированная</span>
					</li>
					<li>
						<span class="left">Переплет: </span>
						<span class="right">проклейка термолистом</span>
					</li>
					<li class="alt">
						<span class="left">Страницы:</span>
						<span class="right">фотобумага</span>
					</li>
					<li>
						<span class="left">Печать:</span>
						<span class="right">фотопечать</span>
					</li>
					<li class="alt">
						<span class="left">Количество разворотов: </span>
						<span class="right">от 10 до 30</span>
					</li>
					<li>
						<span class="left">Разрешение печати: </span>
						<span class="right">300 dpi</span>
					</li>
					<li class="alt">
						<span class="left">Выполение заказа:</span>
						<span class="right">2 дня</span>
					</li>
				</ul>
			</div>
        </div>
    </div>
</div>