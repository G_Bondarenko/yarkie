<div class="content">
	<div class="wrapper">
		<h1>Описание / Заголовок 1 уровня</h1>
		<p>
			Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни...<br>
			Вы ведь тоже, как и мы, любите фотографировать? :-) Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних<br>
			приключений и получайте на них скидки! Только до 16 июля каждая вторая фотокнига (одинаковая по формату и количеству страниц)<br>
			будет стоить на 50% меньше!
		</p>
		<p class="p__date">Действие акции: 02.07.2015 c 12:00 (МСК) - 16.07.2015 до 12:00 (МСК)</p>
		<h3>Заголовок для списка / заголовок 3 уровня</h3>
		<ul>
			<li>Оформите от двух и <a href="">более одинаковых</a> по типу, формату и количеству страниц фотокниг;</li>
			<li>Используйте разные фоны, рамки, украшения или готовые дизайнерские стили для оформления книг;</li>
			<li>По очереди положите все книги в «Корзину» и начните оформлять заказ;</li>
			<li>В процессе оформления стоимость каждой второй книги в заказе уменьшится на 50%!</li>
		</ul>
		<table>
			<tbody>
				<tr class="thead">
					<th class="format" rowspan="2">Формат</th>
					<th class="do" colspan="2">Дообрезной размер (мм.)</th>
					<th class="post" colspan="2">Послеобрезной размер (мм.)</th>
					<th class="kor">Корешок (мм.)</th>
				</tr>
				<tr>
					<td class="do1 th">Развороты</td>
					<td class="do2 th">Обложки</td>
					<td class="post1 th">Развороты</td>
					<td class="post2 th">Обложки</td>
					<td class="kor th">по числу разворотов</td>
				</tr>
				<tr class="alt">
					<td class="format">20х20</td>
					<td class="do1">400х203</td>
					<td class="do2">476.4х240</td>
					<td class="post1">400х203</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr>
					<td class="format">30х20</td>
					<td class="do1">400х270</td>
					<td class="do2"></td>
					<td class="post1">400х270</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr class="alt">
					<td class="format">20х30</td>
					<td class="do1">400х203</td>
					<td class="do2">476.4х240</td>
					<td class="post1">400х203</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
				<tr>
					<td class="format">40х20</td>
					<td class="do1">400х270</td>
					<td class="do2"></td>
					<td class="post1">400х270</td>
					<td class="post2"></td>
					<td class="kor">14.4-32.4</td>
				</tr>
			</tbody>
		</table>
		<ul class="mobile__display table">
			<li>
				<div class="table__title">Формат 20x20</div>
				<div class="table__info">
					<div class="table__info__row alt">Дообрезной размер (мм.)</div>
					<ul>
						<li>Развороты</li>
						<li>Обложки</li>
						<li class="alt">400х203</li>
						<li class="alt">476.4х240</li>
					</ul>
				</div>
			</li>
			<li class="active">
				<div class="table__title">Формат 20x30</div>
				<div class="table__info">
					<div class="table__info__row alt">Дообрезной размер (мм.)</div>
					<ul>
						<li>Развороты</li>
						<li>Обложки</li>
						<li class="alt">400х203</li>
						<li class="alt">476.4х240</li>
					</ul>
				</div>
			</li>
			<li>
				<div class="table__title">Формат 20x40</div>
				<div class="table__info">
					<div class="table__info__row alt">Дообрезной размер (мм.)</div>
					<ul>
						<li>Развороты</li>
						<li>Обложки</li>
						<li class="alt">400х203</li>
						<li class="alt">476.4х240</li>
					</ul>
				</div>
			</li>
		</ul>
		<p>
			Лето в полном разгаре! Пришла пора морского отпуска, самых удивительных путешествий и дачного отдыха в выходные дни...<br>
			Вы ведь тоже, как и мы, любите фотографировать? :-) Тогда скорее создавайте свои красочные фотокниги с фотоснимками своих летних<br>
			приключений и получайте на них скидки! 
		</p>
		<div class="important">
			Выделение текстового блока для особо важной информации в две строки
		</div>
		<div class="mobile__hide">
			<a href="#" class="a__butt">ПОДРОБНЕЕ</a>
		</div>
	</div>
</div>