<div class="article__block article__block-photo mobile__hide">
  <img src="images/bg/img6.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Качество нашей продукции</h2>
                <p>
                    Мы заботимся о том, чтобы вы получили фототовары такими, 
                    как ожидали: в отличном качестве, в целости и сохранности, точно 
                    в срок. Поэтому мы контролируем не только процесс производства, 
                    но и способ упаковки и доставки.
                </p>
                <div>
                    <a href="#" class="readmore a__butt">ПОДРОБНЕЕ</a>
                </div>
            </div>
        </div>
    </div>
</div>