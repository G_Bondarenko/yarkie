<div class="calendar__list without calendar__list3 mobile__hide">
    <div class="wrapper">
        <ul>
            <li class="item1">
                <div class="calendar__icon icon6"></div>
                <a href="#">Подарки по поводу</a>
            </li>
            <li class="item2">
                <div class="calendar__icon icon7"></div>
                <a href="#">Сувениры с фотографией</a>
            </li>
            <li class="active item3">
                <div class="calendar__icon icon8"></div>
                <a href="#">Сувениры с фирменной символикой</a>
            </li>
        </ul>
    </div>
</div>
<div class="mobile__display calendar__list-select">
	<div class="wrapper">
		<select>
			<option value="0">Выбрать размер</option>
			<option value="item1">Подарки по поводу</option>
			<option value="item2">Сувениры с фотографией</option>
			<option value="item3">Сувениры с фирменной символикой</option>
		</select>
	</div>
</div>