<div id="fancybox__reg" class="popup">
    <div class="popup__inner">
        <h2 class="text__center">
            Зарегистрироваться за 10 секунд
        </h2>
        <div class="reg__auth__restore__content">
            <form action="#">

                <div class="input__parent">
                    <input class="custom__input" placeholder="Укажите E-mail" type="text" name="email" id=""/>
                </div>

                <div class="input__parent">
                    <input class="custom__input" placeholder="Укажите телефон (не обязательно)" type="text" name="phone-num" id=""/>
                </div>

                <div class="input__parent checkbox">
                    <input class="custom__checkbox" type="checkbox" name="rules" id="rules"/>
                    <label for="rules">
                        <span class="checkbox__pic"></span>
                        <span class="label__text">Регистрируясь, вы принимаете условия <a href="#">Пользовательского соглашения</a></span>
                    </label>
                </div>

                <div class="input__parent submit">
                    <button class="a__butt" type="submit">
                        Войти
                    </button>
                </div>
            </form>
        </div>
        <div class="bottom__content">
            <h3>
                Войти с помощью
            </h3>
            <ul class="socials__auth">
                <li><a href="#"><img src="img/vk.png" alt=""/></a></li>
                <li><a href="#"><img src="img/fb.png" alt=""/></a></li>
                <li><a href="#"><img src="img/mail.png" alt=""/></a></li>
                <li><a href="#"><img src="img/ya.png" alt=""/></a></li>
                <li><a href="#"><img src="img/google.png" alt=""/></a></li>
                <li><a href="#fancybox__open__id" class="fancybox__restore__openid"><img src="img/id.png" alt=""/></a></li>
            </ul>
            <div class="with__us">
                <p>Уже с нами? <a href="#">Войти</a></p>
            </div>
        </div>
    </div>
</div>


<div id="fancybox__auth" class="popup">
    <div class="popup__inner">
        <h2 class="text__center">
            Вход в систему netPrint.ru
        </h2>
        <div class="reg__auth__restore__content">
            <form action="#">

                <div class="input__parent">
                    <input class="custom__input" placeholder="Укажите E-mail" type="text" name="email" id=""/>
                </div>

                <div class="input__parent">
                    <input class="custom__input" placeholder="Укажите телефон (не обязательно)" type="text" name="phone-num" id=""/>
                </div>

                <div class="input__parent checkbox">
                    <input class="custom__checkbox" type="checkbox" name="rules" id="rules"/>
                    <label for="rules">
                        <span class="checkbox__pic"></span>
                        <span class="label__text">Запомнить меня</span>
                    </label>
                </div>

                <div class="input__parent submit links">
                    <a href="#fancybox__restore" class="fancybox__restore__openid">Напомнить пароль</a>
                    <button class="a__butt" type="submit">
                        Войти
                    </button>
                    <a href="#fancybox__reg" class="fancybox__reg">Регистрация на сайте</a>
                </div>
            </form>
        </div>
        <div class="bottom__content">
            <h3>
                Войти с помощью
            </h3>
            <ul class="socials__auth">
                <li><a href="#"><img src="img/vk.png" alt=""/></a></li>
                <li><a href="#"><img src="img/fb.png" alt=""/></a></li>
                <li><a href="#"><img src="img/mail.png" alt=""/></a></li>
                <li><a href="#"><img src="img/ya.png" alt=""/></a></li>
                <li><a href="#"><img src="img/google.png" alt=""/></a></li>
                <li><a href="#fancybox__open__id" class="fancybox__restore__openid"><img src="img/id.png" alt=""/></a></li>
            </ul>
        </div>
    </div>
</div>

<div id="fancybox__restore" class="popup">
    <div class="popup__inner">
        <h2 class="text__center">
            Восстановление пароля
        </h2>
        <p>Укажите E-mail, использованный при регистрации</p>
        <div class="reg__auth__restore__content">
            <form action="#">
                <div class="input__parent">
                    <input class="custom__input" placeholder="E-mail" type="text" name="email" id=""/>
                </div>
                <div class="input__parent submit links">
                    <button class="a__butt" type="submit">
                        Отправить
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="fancybox__open__id" class="popup">
    <div class="popup__inner">
        <h2 class="text__center">
            OpenID авторизация
        </h2>
        <p>Укажите ваш OpenID для входа на сайт</p>
        <div class="reg__auth__restore__content">
            <form action="#">
                <div class="input__parent">
                    <input class="custom__input" placeholder="OpenID" type="text" name="email" id=""/>
                </div>
                <div class="input__parent submit links">
                    <button class="a__butt" type="submit">
                        Отправить
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>