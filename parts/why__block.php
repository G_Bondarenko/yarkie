<div class="why__block">
    <div class="wrapper">
        <h2>Почему Netprint?</h2>
        <ul>
            <li>
                <img src="images/icons/icon1.png" alt="">
                <h4>100% гарантия</h4>
                <p>что вы останетесь довольны</p>
            </li>
            <li>
                <img src="images/icons/icon2.png" alt="">
                <h4>Самые выгодные цены</h4>
                <p>в регионе и в стране</p>
            </li>
            <li>
                <img src="images/icons/icon3.png" alt="">
                <h4>Отменное качество</h4>
                <p>изготовляемой продукции</p>
            </li>
            <li>
                <img src="images/icons/icon4.png" alt="">
                <h4>Мы можем напечатать</h4>
                <p>20 000 фото за 5минут</p>
            </li>
            <li>
                <img src="images/icons/icon5.png" alt="">
                <h4>Доставим <br>по России</h4>
                <p>Ваш заказ</p>
            </li>
            <li>
                <img src="images/icons/icon6.png" alt="">
                <h4>Редакторы</h4>
                <p>фотокниг, фотосувениров и фотографий</p>
            </li>
        </ul>
    </div>
</div>