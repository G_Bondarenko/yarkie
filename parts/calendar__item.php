<div class="calendar__item">
  	<div class="item1 item">   		   	
		<div class="wrapper">
			<h1>
				Настольные фотокалендари
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_1.jpg" alt="" class="mobile__hide"></a>
							<a href=""><img src="images/slider/img11_1_mobile.jpg" alt="" class="mobile__display"></a>
						</div>
						<a href="#">А5 домик</a>
						<p>
							Ваши любимые фотографии на красочной
							обложке и 12 страницах – это каждый месяц
							счастливая улыбка близких, лучшие
							моменты путешествий.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_2.jpg" alt=""></a>
						</div>
						<a href="#">Планинг</a>
						<p>
							Небольшой настольный календарь, где
							помещается самое важное — главные
							заметки на месяц и самые любимые снимки
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
						   <a href=""> <img src="images/slider/img11_3.jpg" alt=""></a>
						</div>
						<a href="#">Карманный</a>
						<p>
							Лучший вариант для портмоне или
							блокнота — «самый карманный» формат
							календаря. Одно фото или коллаж
							и календарь на год на обороте.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_4.jpg" alt=""></a>
						</div>
						<a href="#">Кухонный</a>
						<p>
							Компактный размер календаря позволит
							вам разместить его даже на самой
							маленькой кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_5.jpg" alt=""></a>
						</div>
						<a href="#">А4</a>
						<p>
							Перекидной настенный календарь
							формата альбомного листа легко вмещает
							до 4-5 фотографий на каждой из 13 страниц.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_6.jpg" alt=""></a>
						</div>
						<a href="#">А3</a>
						<p>
							На 13 листах календаря — ваши самые
							ценные фотографии + календарные сетки.
							И ещё есть достаточно места для ваших
							комментариев .
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_7.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум</a>
						<p>
							Ваши лучшие фотографии напечатанные
							на 13 листах фотобумаги классическим
							способом — то, что нужно для красивого
							и полезного сувенира.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_8.jpg" alt=""></a>
						</div>
						<a href="#">А3 Роял</a>
						<p>
							Перекидной календарь самого популярного
							формата А3: каждый месяц — новая
							классная фотография и самые приятные
							воспоминания и впечатления.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_9.jpg" alt=""></a>
						</div>
						<a href="#">А3 Плакат</a>
						<p>
							Настенный календарь на одном листе
							с календарной сеткой на весь год
							Оформленный в одном из тематических
							стилей, он станет полезным и актуальным
							акцентом комнаты или офиса.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_10.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум плакат</a>
						<p>
							Календарь-плакат формата А3 печатается
							на плотной фотобумаге класса Премиум,
							отчего изображение выглядит особенно
							ярким, чётким и живым.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_11.jpg" alt=""></a>
						</div>
						<a href="#">А2 раскладной</a>
						<p>
							Добавьте в календарную сетку фотографии
							или отметки о важных событиях, и тогда
							календарь станет не только красивым,
							но и полезным аксессуаром в любом
							доме или офисе.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_12.jpg" alt=""></a>
						</div>
						<a href="#">А2 перекидной</a>
						<p>
							Календари А2 на пружине — максимум
							фотографий, максимум впечатлений.
							13 полноформатных страниц календаря,
							потрясающие фотографии, удобные
							календарные сетки.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
    <div class="item2 item">   		   	
		<div class="wrapper">
			<h1>
				Малые фотокалендари
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_1.jpg" alt="" class="mobile__hide"></a>
							<a href=""><img src="images/slider/img11_1_mobile.jpg" alt="" class="mobile__display"></a>
						</div>
						<a href="#">А5 домик</a>
						<p>
							Ваши любимые фотографии на красочной
							обложке и 12 страницах – это каждый месяц
							счастливая улыбка близких, лучшие
							моменты путешествий.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_2.jpg" alt=""></a>
						</div>
						<a href="#">Планинг</a>
						<p>
							Небольшой настольный календарь, где
							помещается самое важное — главные
							заметки на месяц и самые любимые снимки
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
						   <a href=""> <img src="images/slider/img11_3.jpg" alt=""></a>
						</div>
						<a href="#">Карманный</a>
						<p>
							Лучший вариант для портмоне или
							блокнота — «самый карманный» формат
							календаря. Одно фото или коллаж
							и календарь на год на обороте.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_4.jpg" alt=""></a>
						</div>
						<a href="#">Кухонный</a>
						<p>
							Компактный размер календаря позволит
							вам разместить его даже на самой
							маленькой кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_5.jpg" alt=""></a>
						</div>
						<a href="#">А4</a>
						<p>
							Перекидной настенный календарь
							формата альбомного листа легко вмещает
							до 4-5 фотографий на каждой из 13 страниц.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_6.jpg" alt=""></a>
						</div>
						<a href="#">А3</a>
						<p>
							На 13 листах календаря — ваши самые
							ценные фотографии + календарные сетки.
							И ещё есть достаточно места для ваших
							комментариев .
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_7.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум</a>
						<p>
							Ваши лучшие фотографии напечатанные
							на 13 листах фотобумаги классическим
							способом — то, что нужно для красивого
							и полезного сувенира.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_8.jpg" alt=""></a>
						</div>
						<a href="#">А3 Роял</a>
						<p>
							Перекидной календарь самого популярного
							формата А3: каждый месяц — новая
							классная фотография и самые приятные
							воспоминания и впечатления.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_9.jpg" alt=""></a>
						</div>
						<a href="#">А3 Плакат</a>
						<p>
							Настенный календарь на одном листе
							с календарной сеткой на весь год
							Оформленный в одном из тематических
							стилей, он станет полезным и актуальным
							акцентом комнаты или офиса.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_10.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум плакат</a>
						<p>
							Календарь-плакат формата А3 печатается
							на плотной фотобумаге класса Премиум,
							отчего изображение выглядит особенно
							ярким, чётким и живым.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_11.jpg" alt=""></a>
						</div>
						<a href="#">А2 раскладной</a>
						<p>
							Добавьте в календарную сетку фотографии
							или отметки о важных событиях, и тогда
							календарь станет не только красивым,
							но и полезным аксессуаром в любом
							доме или офисе.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_12.jpg" alt=""></a>
						</div>
						<a href="#">А2 перекидной</a>
						<p>
							Календари А2 на пружине — максимум
							фотографий, максимум впечатлений.
							13 полноформатных страниц календаря,
							потрясающие фотографии, удобные
							календарные сетки.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
   <div class="item3 item">   		   	
		<div class="wrapper">
			<h1>
				Большие фотокалендари
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_1.jpg" alt="" class="mobile__hide"></a>
							<a href=""><img src="images/slider/img11_1_mobile.jpg" alt="" class="mobile__display"></a>
						</div>
						<a href="#">А5 домик</a>
						<p>
							Ваши любимые фотографии на красочной
							обложке и 12 страницах – это каждый месяц
							счастливая улыбка близких, лучшие
							моменты путешествий.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_2.jpg" alt=""></a>
						</div>
						<a href="#">Планинг</a>
						<p>
							Небольшой настольный календарь, где
							помещается самое важное — главные
							заметки на месяц и самые любимые снимки
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
						   <a href=""> <img src="images/slider/img11_3.jpg" alt=""></a>
						</div>
						<a href="#">Карманный</a>
						<p>
							Лучший вариант для портмоне или
							блокнота — «самый карманный» формат
							календаря. Одно фото или коллаж
							и календарь на год на обороте.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_4.jpg" alt=""></a>
						</div>
						<a href="#">Кухонный</a>
						<p>
							Компактный размер календаря позволит
							вам разместить его даже на самой
							маленькой кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_5.jpg" alt=""></a>
						</div>
						<a href="#">А4</a>
						<p>
							Перекидной настенный календарь
							формата альбомного листа легко вмещает
							до 4-5 фотографий на каждой из 13 страниц.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_6.jpg" alt=""></a>
						</div>
						<a href="#">А3</a>
						<p>
							На 13 листах календаря — ваши самые
							ценные фотографии + календарные сетки.
							И ещё есть достаточно места для ваших
							комментариев .
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_7.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум</a>
						<p>
							Ваши лучшие фотографии напечатанные
							на 13 листах фотобумаги классическим
							способом — то, что нужно для красивого
							и полезного сувенира.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_8.jpg" alt=""></a>
						</div>
						<a href="#">А3 Роял</a>
						<p>
							Перекидной календарь самого популярного
							формата А3: каждый месяц — новая
							классная фотография и самые приятные
							воспоминания и впечатления.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_9.jpg" alt=""></a>
						</div>
						<a href="#">А3 Плакат</a>
						<p>
							Настенный календарь на одном листе
							с календарной сеткой на весь год
							Оформленный в одном из тематических
							стилей, он станет полезным и актуальным
							акцентом комнаты или офиса.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_10.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум плакат</a>
						<p>
							Календарь-плакат формата А3 печатается
							на плотной фотобумаге класса Премиум,
							отчего изображение выглядит особенно
							ярким, чётким и живым.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_11.jpg" alt=""></a>
						</div>
						<a href="#">А2 раскладной</a>
						<p>
							Добавьте в календарную сетку фотографии
							или отметки о важных событиях, и тогда
							календарь станет не только красивым,
							но и полезным аксессуаром в любом
							доме или офисе.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_12.jpg" alt=""></a>
						</div>
						<a href="#">А2 перекидной</a>
						<p>
							Календари А2 на пружине — максимум
							фотографий, максимум впечатлений.
							13 полноформатных страниц календаря,
							потрясающие фотографии, удобные
							календарные сетки.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
   <div class="item4 item active">   		   	
		<div class="wrapper">
			<h1>
				Средние фотокалендари
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_1.jpg" alt="" class="mobile__hide"></a>
							<a href=""><img src="images/slider/img11_1_mobile.jpg" alt="" class="mobile__display"></a>
						</div>
						<a href="#">А5 домик</a>
						<p>
							Ваши любимые фотографии на красочной
							обложке и 12 страницах – это каждый месяц
							счастливая улыбка близких, лучшие
							моменты путешествий.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_2.jpg" alt=""></a>
						</div>
						<a href="#">Планинг</a>
						<p>
							Небольшой настольный календарь, где
							помещается самое важное — главные
							заметки на месяц и самые любимые снимки
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
						   <a href=""> <img src="images/slider/img11_3.jpg" alt=""></a>
						</div>
						<a href="#">Карманный</a>
						<p>
							Лучший вариант для портмоне или
							блокнота — «самый карманный» формат
							календаря. Одно фото или коллаж
							и календарь на год на обороте.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_4.jpg" alt=""></a>
						</div>
						<a href="#">Кухонный</a>
						<p>
							Компактный размер календаря позволит
							вам разместить его даже на самой
							маленькой кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_5.jpg" alt=""></a>
						</div>
						<a href="#">А4</a>
						<p>
							Перекидной настенный календарь
							формата альбомного листа легко вмещает
							до 4-5 фотографий на каждой из 13 страниц.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_6.jpg" alt=""></a>
						</div>
						<a href="#">А3</a>
						<p>
							На 13 листах календаря — ваши самые
							ценные фотографии + календарные сетки.
							И ещё есть достаточно места для ваших
							комментариев .
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_7.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум</a>
						<p>
							Ваши лучшие фотографии напечатанные
							на 13 листах фотобумаги классическим
							способом — то, что нужно для красивого
							и полезного сувенира.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_8.jpg" alt=""></a>
						</div>
						<a href="#">А3 Роял</a>
						<p>
							Перекидной календарь самого популярного
							формата А3: каждый месяц — новая
							классная фотография и самые приятные
							воспоминания и впечатления.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_9.jpg" alt=""></a>
						</div>
						<a href="#">А3 Плакат</a>
						<p>
							Настенный календарь на одном листе
							с календарной сеткой на весь год
							Оформленный в одном из тематических
							стилей, он станет полезным и актуальным
							акцентом комнаты или офиса.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_10.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум плакат</a>
						<p>
							Календарь-плакат формата А3 печатается
							на плотной фотобумаге класса Премиум,
							отчего изображение выглядит особенно
							ярким, чётким и живым.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_11.jpg" alt=""></a>
						</div>
						<a href="#">А2 раскладной</a>
						<p>
							Добавьте в календарную сетку фотографии
							или отметки о важных событиях, и тогда
							календарь станет не только красивым,
							но и полезным аксессуаром в любом
							доме или офисе.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_12.jpg" alt=""></a>
						</div>
						<a href="#">А2 перекидной</a>
						<p>
							Календари А2 на пружине — максимум
							фотографий, максимум впечатлений.
							13 полноформатных страниц календаря,
							потрясающие фотографии, удобные
							календарные сетки.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
   <div class="item5 item">   		   	
		<div class="wrapper">
			<h1>
				Одностраничные фотокалендари
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_1.jpg" alt="" class="mobile__hide"></a>
							<a href=""><img src="images/slider/img11_1_mobile.jpg" alt="" class="mobile__display"></a>
						</div>
						<a href="#">А5 домик</a>
						<p>
							Ваши любимые фотографии на красочной
							обложке и 12 страницах – это каждый месяц
							счастливая улыбка близких, лучшие
							моменты путешествий.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_2.jpg" alt=""></a>
						</div>
						<a href="#">Планинг</a>
						<p>
							Небольшой настольный календарь, где
							помещается самое важное — главные
							заметки на месяц и самые любимые снимки
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
						   <a href=""> <img src="images/slider/img11_3.jpg" alt=""></a>
						</div>
						<a href="#">Карманный</a>
						<p>
							Лучший вариант для портмоне или
							блокнота — «самый карманный» формат
							календаря. Одно фото или коллаж
							и календарь на год на обороте.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_4.jpg" alt=""></a>
						</div>
						<a href="#">Кухонный</a>
						<p>
							Компактный размер календаря позволит
							вам разместить его даже на самой
							маленькой кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_5.jpg" alt=""></a>
						</div>
						<a href="#">А4</a>
						<p>
							Перекидной настенный календарь
							формата альбомного листа легко вмещает
							до 4-5 фотографий на каждой из 13 страниц.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_6.jpg" alt=""></a>
						</div>
						<a href="#">А3</a>
						<p>
							На 13 листах календаря — ваши самые
							ценные фотографии + календарные сетки.
							И ещё есть достаточно места для ваших
							комментариев .
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_7.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум</a>
						<p>
							Ваши лучшие фотографии напечатанные
							на 13 листах фотобумаги классическим
							способом — то, что нужно для красивого
							и полезного сувенира.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_8.jpg" alt=""></a>
						</div>
						<a href="#">А3 Роял</a>
						<p>
							Перекидной календарь самого популярного
							формата А3: каждый месяц — новая
							классная фотография и самые приятные
							воспоминания и впечатления.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_9.jpg" alt=""></a>
						</div>
						<a href="#">А3 Плакат</a>
						<p>
							Настенный календарь на одном листе
							с календарной сеткой на весь год
							Оформленный в одном из тематических
							стилей, он станет полезным и актуальным
							акцентом комнаты или офиса.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_10.jpg" alt=""></a>
						</div>
						<a href="#">А3 Премиум плакат</a>
						<p>
							Календарь-плакат формата А3 печатается
							на плотной фотобумаге класса Премиум,
							отчего изображение выглядит особенно
							ярким, чётким и живым.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_11.jpg" alt=""></a>
						</div>
						<a href="#">А2 раскладной</a>
						<p>
							Добавьте в календарную сетку фотографии
							или отметки о важных событиях, и тогда
							календарь станет не только красивым,
							но и полезным аксессуаром в любом
							доме или офисе.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img11_12.jpg" alt=""></a>
						</div>
						<a href="#">А2 перекидной</a>
						<p>
							Календари А2 на пружине — максимум
							фотографий, максимум впечатлений.
							13 полноформатных страниц календаря,
							потрясающие фотографии, удобные
							календарные сетки.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="page__text">
    <div class="wrapper">
        <h1>Заголовок для текста</h1>
        <p>
            Начните год в стиле с персональным фото-календарем от netPrint. Наши настраиваемые календари идеально подходят для записи встреч,
            особых случаев, или отслеживания то, что происходит в вашей семье. У нас есть огромный диапазон размеров и доступных форматов,
            а также вы можете выбрать свой собственный месяц начала, идеально подходит для молодоженов или в школу, используя наши
            индивидуальные школьные дневники.
        </p>
    </div>
</div>