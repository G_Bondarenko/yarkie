<div class="article__block article__block-photo block__gal">
  <img src="images/bg/img11.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Фотокниги</h2>
                <p>
                    Фотокнига для самых значимых фотографий, которые стоит
					передать по наследству внукам. Фотопечать не померкнет и через
					100 лет, а особо плотные страницы и обложка надежно защитят ваши
					фотографии от повреждений.
                </p>
            </div>
        </div>
    </div>
</div>