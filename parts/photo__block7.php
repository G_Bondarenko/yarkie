<div class="article__block article__block-photo block__gal">
  <img src="images/bg/img7.jpg" class="slider__bg" alt="">
   <div class="wrapper">        
        <div class="vertical__align">
            <div class="article__block-info">
                <h2>Фотокалендари</h2>
                <p>
                    Фотокалендарь – оригинальный и, главное, неповторимый подарок!
                    Фотоцентр PIXART дарит возможность сделать календарь с фото каждому!
                    Обрадуйте друзей и родных уникальным сюрпризом, который
                    на протяжении года будет лучшим напоминанием о Вас!
                </p>
            </div>
        </div>
    </div>
</div>