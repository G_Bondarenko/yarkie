<head>
	<!-- Basic Page Needs -->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta charset="utf-8">
	<title>netPrint</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="lomova marina">

	<!-- Mobile Specific Metas-->
	<meta name="viewport" content="width=device-width">
	<meta content="telephone=no" name="format-detection">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,700,400&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link href="css/font-awesome.min.css" rel="stylesheet">

	<link href="css/reset.css" rel="stylesheet">
	<link href="css/jquery.formstyler.css" rel="stylesheet">
	<link href="css/jquery.bxslider.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">
	<link href="css/owl.transitions.css" rel="stylesheet">
	<link href="css/jquery.fancybox.css" rel="stylesheet">
	<link href="css/template.css" rel="stylesheet">
	<link href="css/adaptation.css" rel="stylesheet" media="screen">

	<!--[if lt IE 9]>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>	
	<![endif]-->

	<!--[if lt IE 9]>
		<link href="css/ie8.css" rel="stylesheet">
	<![endif]-->
</head>