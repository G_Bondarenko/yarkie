<div class="calendar__item">
  	<div class="item1 item">   		   	
		<div class="wrapper">
			<h1>
				Подарки по поводу
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_1.jpg" alt=""></a>
						</div>
						<a href="#">Фотокалендари</a>
						<p>
							Украшайте каждую страницу календаря
							вашими любимыми фотографиями,
							выбирайте дизайнерские стили
							оформления
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_2.jpg" alt=""></a>
						</div>
						<a href="#">Фототетради и фотоблокноты</a>
						<p>
							Теперь вы можете заказать у нас тетрадь
							или блокнот с фотографиями из вашего
							фотоархива на обложке и вашим текстом.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_3.jpg" alt=""></a>
						</div>
						<a href="#">Фотокружки пивные</a>
						<p>
							Пивная кружка с вашей фотографией.
							Пожалуй, самый актуальный подарок для
							любителей пенного напитка, и не только.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_4.jpg" alt=""></a>
						</div>
						<a href="#">Фотокружки</a>
						<p>
							Фотокружка - оригинальный подарок.
							Уникален тем, что ему будут рады все без
							исключения.  Для оформления выбирайте
							ваши любимые фотографии.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_5.jpg" alt=""></a>
						</div>
						<a href="#">Фотофутболки</a>
						<p>
							Выделяйтесь, будьте оригинальными!
							Заказывайте печать на футболках с вашими
							любимыми фотографиями! Для себя
							и близких вам людей.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_6.jpg" alt=""></a>
						</div>
						<a href="#">Бесплатные фотовизитки</a>
						<p>
							Фотовизитки в фирменном стиле - так мы
							назвали визитки в новом, специальном
							дизайне от netPrint.ru. На обратной стороне
							каждой визитки - фирменный фон netPrint.ru
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_7.jpg" alt=""></a>
						</div>
						<a href="#">Фотовизитки</a>
						<p>
							Персональные фотовизитки  - это решение
							для бизнеса и повседневной жизни.
							Двусторонняя печать.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_8.jpg" alt=""></a>
						</div>
						<a href="#">Фотопостеры</a>
						<p>
							Закажите постер со своей фотографией
							и текстом в оригинальном оформлении.
							Порадуйте своих близких и любимых
							красивым и необычным подарком
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_9.jpg" alt=""></a>
						</div>
						<a href="#">Фотооткрытки и приглашения</a>
						<p>
							Фотооткрытка станет отличным
							дополнением к подарку. А оригинальное
							приглашение ваши гости оценят
							по достоинству
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_10.jpg" alt=""></a>
						</div>
						<a href="#">Фотомагниты</a>
						<p>
							Превратите ваш холодильник в настоящую
							фотогалерею! Оригинальные фотомагниты
							станут любимым украшением на вашей
							кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_11.jpg" alt=""></a>
						</div>
						<a href="#">Фотохолсты</a>
						<p>
							Необычный и очень эффектный способ
							демонстрации ваших фоторабот. Снимки
							печатаются на холстах разного формата
							и поставляются вместе с крепежным
							набором.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_12.jpg" alt=""></a>
						</div>
						<a href="#">Фотопазлы</a>
						<p>
							Предлагаем вам необычную идею
							памятного подарка – фотопазл. 5 форматов
							пазлов, из 24, 50, 204, 408 и 1000 элементов.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
  	<div class="item2 item">   		   	
		<div class="wrapper">
			<h1>
				Сувениры с фотографией
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_1.jpg" alt=""></a>
						</div>
						<a href="#">Фотокалендари</a>
						<p>
							Украшайте каждую страницу календаря
							вашими любимыми фотографиями,
							выбирайте дизайнерские стили
							оформления
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_2.jpg" alt=""></a>
						</div>
						<a href="#">Фототетради и фотоблокноты</a>
						<p>
							Теперь вы можете заказать у нас тетрадь
							или блокнот с фотографиями из вашего
							фотоархива на обложке и вашим текстом.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_3.jpg" alt=""></a>
						</div>
						<a href="#">Фотокружки пивные</a>
						<p>
							Пивная кружка с вашей фотографией.
							Пожалуй, самый актуальный подарок для
							любителей пенного напитка, и не только.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_4.jpg" alt=""></a>
						</div>
						<a href="#">Фотокружки</a>
						<p>
							Фотокружка - оригинальный подарок.
							Уникален тем, что ему будут рады все без
							исключения.  Для оформления выбирайте
							ваши любимые фотографии.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_5.jpg" alt=""></a>
						</div>
						<a href="#">Фотофутболки</a>
						<p>
							Выделяйтесь, будьте оригинальными!
							Заказывайте печать на футболках с вашими
							любимыми фотографиями! Для себя
							и близких вам людей.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_6.jpg" alt=""></a>
						</div>
						<a href="#">Бесплатные фотовизитки</a>
						<p>
							Фотовизитки в фирменном стиле - так мы
							назвали визитки в новом, специальном
							дизайне от netPrint.ru. На обратной стороне
							каждой визитки - фирменный фон netPrint.ru
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_7.jpg" alt=""></a>
						</div>
						<a href="#">Фотовизитки</a>
						<p>
							Персональные фотовизитки  - это решение
							для бизнеса и повседневной жизни.
							Двусторонняя печать.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_8.jpg" alt=""></a>
						</div>
						<a href="#">Фотопостеры</a>
						<p>
							Закажите постер со своей фотографией
							и текстом в оригинальном оформлении.
							Порадуйте своих близких и любимых
							красивым и необычным подарком
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_9.jpg" alt=""></a>
						</div>
						<a href="#">Фотооткрытки и приглашения</a>
						<p>
							Фотооткрытка станет отличным
							дополнением к подарку. А оригинальное
							приглашение ваши гости оценят
							по достоинству
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_10.jpg" alt=""></a>
						</div>
						<a href="#">Фотомагниты</a>
						<p>
							Превратите ваш холодильник в настоящую
							фотогалерею! Оригинальные фотомагниты
							станут любимым украшением на вашей
							кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_11.jpg" alt=""></a>
						</div>
						<a href="#">Фотохолсты</a>
						<p>
							Необычный и очень эффектный способ
							демонстрации ваших фоторабот. Снимки
							печатаются на холстах разного формата
							и поставляются вместе с крепежным
							набором.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_12.jpg" alt=""></a>
						</div>
						<a href="#">Фотопазлы</a>
						<p>
							Предлагаем вам необычную идею
							памятного подарка – фотопазл. 5 форматов
							пазлов, из 24, 50, 204, 408 и 1000 элементов.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
   	<div class="item3 item active">   		   	
		<div class="wrapper">
			<h1>
				Сувениры с фирменной символикой
			</h1>
		</div>
		<div class="calendar__item-ul">
			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_1.jpg" alt=""></a>
						</div>
						<a href="#">Фотокалендари</a>
						<p>
							Украшайте каждую страницу календаря
							вашими любимыми фотографиями,
							выбирайте дизайнерские стили
							оформления
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_2.jpg" alt=""></a>
						</div>
						<a href="#">Фототетради и фотоблокноты</a>
						<p>
							Теперь вы можете заказать у нас тетрадь
							или блокнот с фотографиями из вашего
							фотоархива на обложке и вашим текстом.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_3.jpg" alt=""></a>
						</div>
						<a href="#">Фотокружки пивные</a>
						<p>
							Пивная кружка с вашей фотографией.
							Пожалуй, самый актуальный подарок для
							любителей пенного напитка, и не только.
						</p>
						<div class="slider__block-price">
							от
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_4.jpg" alt=""></a>
						</div>
						<a href="#">Фотокружки</a>
						<p>
							Фотокружка - оригинальный подарок.
							Уникален тем, что ему будут рады все без
							исключения.  Для оформления выбирайте
							ваши любимые фотографии.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_5.jpg" alt=""></a>
						</div>
						<a href="#">Фотофутболки</a>
						<p>
							Выделяйтесь, будьте оригинальными!
							Заказывайте печать на футболках с вашими
							любимыми фотографиями! Для себя
							и близких вам людей.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_6.jpg" alt=""></a>
						</div>
						<a href="#">Бесплатные фотовизитки</a>
						<p>
							Фотовизитки в фирменном стиле - так мы
							назвали визитки в новом, специальном
							дизайне от netPrint.ru. На обратной стороне
							каждой визитки - фирменный фон netPrint.ru
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>

			<div>
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_7.jpg" alt=""></a>
						</div>
						<a href="#">Фотовизитки</a>
						<p>
							Персональные фотовизитки  - это решение
							для бизнеса и повседневной жизни.
							Двусторонняя печать.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_8.jpg" alt=""></a>
						</div>
						<a href="#">Фотопостеры</a>
						<p>
							Закажите постер со своей фотографией
							и текстом в оригинальном оформлении.
							Порадуйте своих близких и любимых
							красивым и необычным подарком
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_9.jpg" alt=""></a>
						</div>
						<a href="#">Фотооткрытки и приглашения</a>
						<p>
							Фотооткрытка станет отличным
							дополнением к подарку. А оригинальное
							приглашение ваши гости оценят
							по достоинству
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
			<div class="alt">
				<ul class="wrapper">
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_10.jpg" alt=""></a>
						</div>
						<a href="#">Фотомагниты</a>
						<p>
							Превратите ваш холодильник в настоящую
							фотогалерею! Оригинальные фотомагниты
							станут любимым украшением на вашей
							кухне.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_11.jpg" alt=""></a>
						</div>
						<a href="#">Фотохолсты</a>
						<p>
							Необычный и очень эффектный способ
							демонстрации ваших фоторабот. Снимки
							печатаются на холстах разного формата
							и поставляются вместе с крепежным
							набором.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
					<li>
						<div class="calendar__item-img">
							<a href=""><img src="images/slider/img14_12.jpg" alt=""></a>
						</div>
						<a href="#">Фотопазлы</a>
						<p>
							Предлагаем вам необычную идею
							памятного подарка – фотопазл. 5 форматов
							пазлов, из 24, 50, 204, 408 и 1000 элементов.
						</p>
						<div class="slider__block-price">
							<dt>от</dt>
							<span class="new__price">590</span>
						</div>
						<a href="#" class="a__butt">ВЫБРАТЬ</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>