<div class="tovar__info">
    <div class="wrapper clearfix">
        <div class="img__desc">
           <div class="tovar__slider__wrap">
                <ul class="tovar__slider">
                    <li><img src="images/slider/img6_1.jpg" /></li>
                    <li><img src="images/template/clear__photo.jpg" /></li>
                    <li><img src="images/template/clear__photo.jpg" /></li>
                    <li><img src="images/template/clear__photo.jpg" /></li>
                </ul>

                <div id="tovar__slider-pager" class="tovar__slider-pager">
                    <a data-slide-index="0" href=""><img src="images/template/clear__photo.jpg" /></a>
                    <a data-slide-index="1" href=""><img src="images/template/clear__photo.jpg" /></a>
                    <a data-slide-index="2" href=""><img src="images/template/clear__photo.jpg" /></a>
                    <a data-slide-index="3" href=""><img src="images/template/clear__photo.jpg" /></a>
                </div>
            </div>
            <h5>
                Пивная кружка для поклонников пива – это любимая
                и практически именная вещь
            </h5>
            <p>
                Оригинальные фотографий и сугубо мужские стили помогут вам
                создать веселую и выразительную фотокружку для пива, которой
                порадуется каждый любитель этого пенного напитка.
            </p>
        </div>
        <div class="tovar__detail">
            <h1>Фотокружки пивные</h1>
            <div class="tovar__detail-price">
                <dt>от</dt> <span class="new__price">3 200 </span> <span class="old__price">3 600 P</span>
            </div>                        
            
            <div><a href="#popup" class="fancybox a__butt a__submit">ОФОРМИТЬ ТОВАР</a></div>
            
            <ul class="specifications">                
                <li class="alt">
                    <span class="left">Диаметр:</span>
                    <span class="right">7.5 см</span>
                </li>
                <li>
                    <span class="left">Высота:</span>
                    <span class="right">16 см</span>
                </li>
                <li class="alt">
                    <span class="left">Объем:</span>
                    <span class="right">500 мл</span>
                </li>
                <li>
                    <span class="left">Разрешение печати:</span>
                    <span class="right">300 dpi</span>
                </li>
                <li class="alt">
                    <span class="left">Область печати: </span>
                    <span class="right">197х95 мм</span>
                </li>
                <li>
                    <span class="left">Упаковка:</span>
                    <span class="right">картонная коробка с пузырчатой пленкой</span>
                </li>
                <li class="alt">
                    <span class="left">Выполение заказа:</span>
                    <span class="right">2 дня</span>
                </li>
            </ul>
        </div>
    </div>
</div>