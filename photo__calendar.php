<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="calandar" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <?php include('parts/photo__block7.php'); ?>
            <?php include('parts/calendar__list2.php'); ?>
            <?php include('parts/calendar__item.php'); ?>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>