<!doctype html>
<html>
<head>
    <!-- Basic Page Needs -->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="utf-8">
    <title>netPrint</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="lomova marina">

    <!-- Mobile Specific Metas-->
    <!--        <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <meta content="telephone=no" name="format-detection">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,700,400&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link href="css/reset.css" rel="stylesheet">
    <link href="css/jquery.formstyler.css" rel="stylesheet">
    <link href="css/jquery.bxslider.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/jquery.fancybox.css" rel="stylesheet">
    <link href="css/template.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <link href="css/ie8.css" rel="stylesheet">
    <![endif]-->
</head>
<body>
<div id="main" class="page">
    <?php include('parts/header.php'); ?>
    <?php include('parts/breadcrumbs.php'); ?>
    <?php include('parts/txt__page__content.php'); ?>
    <?php include('parts/reg__auth_rertore_forms.php'); ?>
    <?php include('parts/footer.php'); ?>
</div>
</body>

<!-- JavaScript-->
<script src="//api-maps.yandex.ru/2.0-stable/?load=package.standard,package.geoObjects,package.geoQuery&lang=ru-RU" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.10.1.min.js"><\/script>')</script>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
<!-- VK Widget -->
<script type="text/javascript">
    VK.Widgets.Group("vk_groups", {mode: 0, width: "310", height: "259", color1: '4b1543', color2: 'ffffff', color3: '692a60'}, 21434998);
</script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script src="js/jquery.formstyler.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/jquery.liBlockSize.js"></script>
<script src="js/pgwbrowser.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/custom.js"></script>
</html>