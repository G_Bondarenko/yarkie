<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="shop__presents" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <?php include('parts/photo__block10.php'); ?>
            <?php include('parts/calendar__list3.php'); ?>
            <?php include('parts/calendar__item4.php'); ?>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>