<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="action__in" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <?php include('parts/header__slider2.php'); ?>
            <?php include('parts/action__inner.php'); ?>
            <?php include('parts/specials__block2.php'); ?>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>