<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="tovar" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/breadcrumbs.php'); ?>
            <?php include('parts/tovar__info__clear.php'); ?>
            <?php include('parts/technical__sizes.php'); ?>
            <?php include('parts/quality__block2.php'); ?>
            <?php include('parts/photo__block6.php'); ?>
            <?php include('parts/specials__block.php'); ?>
            <?php include('parts/footer.php'); ?>
            <?php include('parts/popup.php'); ?>
            <?php include('parts/technical__form.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>