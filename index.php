<!doctype html>
<html>
    <?php include('parts/head.php'); ?>
    <body>
        <div id="main" class="page">            
            <?php include('parts/header.php'); ?>
            <?php include('parts/header__slider.php'); ?>
            <?php include('parts/specials__block.php'); ?>
            <?php include('parts/photo__block.php'); ?>
            <?php include('parts/services__block.php'); ?>
            <?php include('parts/photo__block2.php'); ?>
            <?php include('parts/calendar__list.php'); ?>
            <?php include('parts/photo__block3.php'); ?>
            <?php include('parts/photo__block4.php'); ?>
            <?php include('parts/photo__block5.php'); ?>
            <?php include('parts/news__block.php'); ?>
            <?php include('parts/why__block.php'); ?>
            <?php include('parts/reg__auth_rertore_forms.php'); ?>
            <?php include('parts/footer.php'); ?>
        </div>
    </body>
    <?php include('parts/js.php'); ?>
</html>